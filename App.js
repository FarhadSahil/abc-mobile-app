import React from "react";
import {
  Text,
  Button,
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
} from "react-native";
import { useNavigation, DrawerActions } from "@react-navigation/native";

import Splash from "./src/screens/Splash";
import Login from "./src/screens/Login";
import SurveyLogin from "./src/screens/SurveyLogin";
import JobDetail from "./src/screens/JobDetail";

import Survey from "./src/screens/Survey";
import Home from "./src/screens/Home";
import Location from "./src/screens/Location";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import LiveLocationTracker from "./src/screens/LiveLocationTracker";

const navigationRef = React.createRef();

function navigate(name, params) {
  navigationRef.current && navigationRef.current.navigate(name, params);
}
const Stack = createStackNavigator();
const NavigationDrawerStructure = (props) => {
  const navigation = useNavigation();
  //Structure for the navigatin Drawer
  const toggleDrawer = () => {
    navigation.dispatch(DrawerActions.toggleDrawer());
  };

  return (
    <View style={{ flexDirection: "row" }}>
      <TouchableOpacity onPress={toggleDrawer}>
        {/*Donute Button Image */}
        <Image
          source={{
            uri: "https://raw.githubusercontent.com/AboutReact/sampleresource/master/drawerWhite.png",
          }}
          style={{ width: 25, height: 25, marginLeft: 5 }}
        />
      </TouchableOpacity>
    </View>
  );
};
export default class App extends React.Component {
  constructor(props) {
    super(props);
  }
  navigateByCond = (path) => {
    if (path == "joblist") {
      navigate("Job List");
    } else if (path == "acceptedjob") {
      navigate("Accept jobs");
    }
  };

  render() {
    return (
      <NavigationContainer ref={navigationRef}>
        <Stack.Navigator>
          <Stack.Screen
            name="Welcome"
            component={Splash}
            options={{
              title: "Welcome",
              headerStyle: {
                backgroundColor: "#f4511e",
              },
              headerTintColor: "#fff",
              headerTitleStyle: {
                fontWeight: "bold",
              },
            }}
          />
          <Stack.Screen
            name="Login"
            component={Login}
            options={{
              title: "Driver Login",
              headerStyle: {
                backgroundColor: "#f4511e",
              },
              headerTintColor: "#fff",
              headerTitleStyle: {
                fontWeight: "bold",
              },
            }}
          />
          <Stack.Screen
            name="Location"
            component={Location}
            options={{
              title: "Find Location",
              headerStyle: {
                backgroundColor: "#f4511e",
              },
              headerTintColor: "#fff",
              headerTitleStyle: {
                fontWeight: "bold",
              },
            }}
          />

          <Stack.Screen
            name="Accepted Job"
            component={Home}
            options={{
              headerLeft: () => (
                <NavigationDrawerStructure navigationProps={this.props} />
              ),
              title: "Home",
              headerStyle: {
                backgroundColor: "#f4511e",
              },
              headerTintColor: "#fff",
              headerTitleStyle: {
                fontWeight: "bold",
              },

              headerRight: () => (
                <View
                  style={{
                    flex: 1,
                    flexDirection: "row",
                    marginHorizontal: 10,
                    justifyContent: "space-between",
                    alignItems: "center",
                  }}
                >
                  <TouchableOpacity
                    onPress={() => this.navigateByCond("acceptedjob")}
                  >
                    <Text>Accepted jobs</Text>
                  </TouchableOpacity>
                  <Text style={styles.txtStyle}></Text>
                  <TouchableOpacity
                    onPress={() => this.navigateByCond("joblist")}
                    title="Job Lists"
                  ></TouchableOpacity>
                </View>
              ),
            }}
          />
          <Stack.Screen
            name="LiveLocationTracker"
            component={LiveLocationTracker}
            options={{
              title: "LiveLocationTracker",
              headerStyle: {
                backgroundColor: "#f4511e",
              },
              headerTintColor: "#fff",
              headerTitleStyle: {
                fontWeight: "bold",
              },
            }}
          />
          <Stack.Screen
            name="JobDetail"
            component={JobDetail}
            options={{
              title: "Job Detail",
              headerStyle: {
                backgroundColor: "#f4511e",
              },
              headerTintColor: "#fff",
              headerTitleStyle: {
                fontWeight: "bold",
              },
            }}
          />
          <Stack.Screen
            name="Survey"
            component={Survey}
            options={{
              title: "Survey form",
              headerStyle: {
                backgroundColor: "#f4511e",
              },
              headerTintColor: "#fff",
              headerTitleStyle: {
                fontWeight: "bold",
              },
            }}
          />
          <Stack.Screen
            name="Survey Login"
            component={SurveyLogin}
            options={{
              title: "Survey Login",
              headerStyle: {
                backgroundColor: "#f4511e",
              },
              headerTintColor: "#fff",
              headerTitleStyle: {
                fontWeight: "bold",
              },
            }}
          />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}
const styles = StyleSheet.create({
  txtStyle: {
    width: 10,
  },
});
