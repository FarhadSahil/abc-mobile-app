import firebase from "firebase/app";

// Optionally import the services that you want to use
import "firebase/auth";
//import "firebase/database";
//import "firebase/firestore";
//import "firebase/functions";
//import "firebase/storage";

// Initialize Firebase
// Initialize Firebase
var firebaseConfig = {
  apiKey: "AIzaSyAqsBWkjLcB-xAtSYJoqOv9LwlwAWAJh8Y",
  authDomain: "push-notification-3ace3.firebaseapp.com",
  projectId: "push-notification-3ace3",
  storageBucket: "push-notification-3ace3.appspot.com",
  messagingSenderId: "1074011205831",
  appId: "1:1074011205831:web:3e197c56c432107f3f0ee2",
  measurementId: "G-NJLPHK13SL",
};

let Firebase;

if (firebase.apps.length === 0) {
  Firebase = firebase.initializeApp(firebaseConfig);
}

export default Firebase;
