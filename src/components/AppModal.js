import React, { useState, useEffect } from "react";
import {
  Alert,
  Modal,
  StyleSheet,
  Text,
  TouchableHighlight,
  View,
  DeviceEventEmitter,
} from "react-native";
import { Audio } from "expo-av";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { BASE_URL } from "../Config";
import { _reject, _detail } from "../functions";
export default function AppModal(props) {
  const [sound, setSound] = useState();
  const [token, setToken] = useState();
  const [userId, setUserId] = useState();
  const [modalVisible, setModalVisible] = useState(false);
  const [loading, setLoading] = useState(false);
  const [lastJob, setLastJob] = useState();
  const { navigation } = props;
  async function playSound() {
    const { sound } = await Audio.Sound.createAsync(
      require("../../assets/beep.mp3")
    );
    setSound(sound);

    await sound.playAsync();
  }

  useEffect(() => {
    AsyncStorage.getItem("token").then((token) => {
      setToken(token);
    });
    AsyncStorage.getItem("user_id").then((user_id) => {
      setUserId(user_id);
    });
    AsyncStorage.getItem("lastJob").then((lastJob) => {
      var lastJob = JSON.parse(lastJob) ?? ""; //{"id":"007","name":"James Bond"}
      setLastJob(lastJob);
      setLoading(true);
    });
    makeModalVisible();
    return sound
      ? () => {
          sound.unloadAsync();
        }
      : undefined;
  }, []);
  function makeModalVisible() {
    setModalVisible(true);
    playSound();
  }
  function action(id, action) {
    if (action == "reject") {
      _rejctJob(id);
    }
    if (action == "detail") {
      setModalVisible(false);
      DeviceEventEmitter.emit("hideComponent", {});
      navigation.navigate("JobDetail", { id: id });
    }
    if (action == "accept") {
      acceptJob(id);
      DeviceEventEmitter.emit("hideComponent", {});
      setModalVisible(false);
    }
  }
  function acceptJob(id) {
    try {
      var OBJECT = {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Origin: "",
          Authorization: "Bearer " + token,
        },
        body: JSON.stringify({
          user_id: userId,
        }),
      };
      fetch(`${BASE_URL}accept-job/${id}`, OBJECT)
        .then(function (res) {
          return res.json();
        })
        .then(res => {
          if(res.danger){
          Alert.alert(
            "Job Already Exist !",
             res.danger
          );
          return;
          }
          navigation.navigate("Job List");
        })
        .catch(function (error) {
          throw error;
        });
    } catch (err) {
      console.log("frad")

    }
  }

  async function _rejctJob(id) {
    var $this = this;
    try {
      var OBJECT = {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Origin: "",
          Authorization: "Bearer " + token,
        },
        body: JSON.stringify({
          user_id: userId,
        }),
      };
      fetch(`${BASE_URL}reject-job/${id}`, OBJECT)
        .then(function (res) {
          return res.json();
        })
        .then((res) => {
          if (res.success == "success") {
            setModalVisible(false);
            navigation.navigate("Job List");
            DeviceEventEmitter.emit("hideComponent", {});
          }
        });
    } catch (err) {
      console.log(err);
    }
  }

  return (
    <View>
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
        }}
      >
        {loading == true && lastJob != "" ? (
          <View style={styles.centeredView}>
            <View style={styles.modalView}>
              <View style={styles.modalFooter}>
                <Text style={styles.txtTitle}>{lastJob.created_at ?? ""}</Text>
              </View>
              <View style={styles.modalFooter}>
                <Text style={[styles.fontColor]}>
                  {lastJob.loading_address ?? ""}-
                  {lastJob.unloading_address ?? ""}
                </Text>
                <Text style={[styles.fontColor]}>
                  {" "}
                  {lastJob.loading_address ?? ""}-{" "}
                  {lastJob.unloading_address ?? ""} £ {lastJob.amount ?? ""}
                </Text>
              </View>

              <View style={styles.modalContent}>
                <Text style={[styles.contentHeader, styles.fontColor]}>
                  {lastJob.vehicle_required ?? ""}
                </Text>
              </View>
              <View style={styles.location}>
                <Text style={[styles.txtSize, styles.fontColor]}>
                  From : {lastJob.from ?? ""}
                </Text>
                <Text style={[styles.txtSize, styles.fontColor]}>
                  To : {lastJob.to ?? ""}
                </Text>
              </View>

              <View>
                <Text style={[styles.txtSize, styles.fontColor]}>
                  Move type : 8 Boxes 7 Lugage
                </Text>
                <Text style={[styles.txtSize, styles.fontColor]}>
                  Self Loaded : {lastJob.field_load ?? ""}
                </Text>
                <Text style={[styles.txtSize, styles.fontColor]}>
                  Adults : {lastJob.text_adults ?? ""}
                </Text>
                <Text style={[styles.txtSize, styles.fontColor]}>
                  Childrens: {lastJob.text_children ?? ""}
                </Text>
              </View>
              <View style={styles.buttonGroup}>
                <TouchableHighlight
                  style={{ ...styles.openButton, backgroundColor: "#5cb85c" }}
                  onPress={() => action(lastJob.id, "accept")}
                >
                  <Text style={styles.textStyle}>Accept</Text>
                </TouchableHighlight>

                <TouchableHighlight
                  style={{ ...styles.openButton, backgroundColor: "#df4759" }}
                  onPress={() => action(lastJob.id, "reject")}
                >
                  <Text style={styles.textStyle}>Reject</Text>
                </TouchableHighlight>
                <TouchableHighlight
                  style={{ ...styles.openButton, backgroundColor: "#2196F3" }}
                  onPress={() => {
                    setModalVisible(false);
                    DeviceEventEmitter.emit("hideComponent", {});
                  }}
                >
                  <Text style={styles.textStyle}>Close</Text>
                </TouchableHighlight>
              </View>
            </View>
          </View>
        ) : (
          <Text></Text>
        )}
      </Modal>
    </View>
  );
}

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22,
    width: 100 + "%",
  },
  modalView: {
    width: 300,
    height: "auto",
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    // alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  openButton: {
    backgroundColor: "#F194FF",
    borderRadius: 20,
    paddingTop: 10,
    paddingBottom: 10,
    elevation: 2,
    marginTop: 10,
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center",
  },
  buttonStyle: {
    display: "none",
  },
  modalFooter: {
    flexDirection: "row",
    justifyContent: "space-between",
    borderBottomWidth: 1,
    paddingBottom: 10,
    borderColor: "#f6f6f6",
  },
  modalContent: {},
  contentHeader: {
    textAlign: "center",
    fontSize: 18,
    paddingBottom: 10,
    paddingTop: 20,
  },
  location: {
    flexDirection: "row",
    justifyContent: "space-between",
    flexWrap: "wrap",
  },
  txtSize: {
    fontSize: 16,
    paddingBottom: 10,
    paddingTop: 10,
  },
  fontColor: {
    color: "grey",
  },
  txtTitle: {
    textAlign: "center",
    fontWeight: "bold",
    color: "black",
    flex: 1,
    justifyContent: "center",
    fontSize: 20,
  },
});
