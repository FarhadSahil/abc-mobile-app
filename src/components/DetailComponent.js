import React, { Component } from "react";
import MapView, { Marker } from "react-native-maps";
import MapViewDirections from "react-native-maps-directions";
import { Snackbar } from "react-native-paper";
// npm start --reset-cache

import {
  View,
  Text,
  ScrollView,
  SafeAreaView,
  TouchableOpacity,
} from "react-native";
import {
  Card,
  ListItem,
  Button,
  Icon,
  Avatar,
  ButtonGroup,
} from "react-native-elements";
import { BASE_URL } from "../Config";
import styles from "../styles/JobDetail";

// LogBox.ignoreLogs(['VirtualizedLists should never be nested']);

import AsyncStorage from "@react-native-async-storage/async-storage";

export default class DetailCompoent extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    if (this.state.loading) {
      this.state.user_id;
    }

    // console.log("loadinng data", this.state.data);
    var from, to, extra_information;

    if (this.state.loading && this.state.data.from != "undefined") {
      from = this.state.data.from;
      to = this.state.data.to;
      extra_information = this.state.data.extra_information;
    }
    if (this.state.loading && this.state.data.data.job != "undefined") {
      // console.log("i am inside the else if condition");
      from = this.state.data.data.job.from;
      to = this.state.data.data.job.to;
      extra_information = this.state.data.data.job.extra_information;
    } else {
      from = "";
      to = "";
      extra_information = "";
    }
    //console.log("from", from);
    // console.log("have a nice day" + JSON.stringify(this.state.data));
    return (
      <SafeAreaView style={styles.container}>
        <ScrollView style={styles.scrollView}>
          <Snackbar
            visible={this.state.sanckbarVisible}
            onDismiss={() => this.setState({ visible: false })}
          >
            Email has been sent.
          </Snackbar>
          {this.state.loading ? (
            <View>
              <MapView
                initialRegion={{
                  latitude: parseFloat(from ? from : 34.0151) || 34.0151,
                  longitude: parseFloat(to ? to : 71.5249) || 71.5249,

                  latitudeDelta: 0.0922,
                  longitudeDelta: 0.0421,
                }}
                style={styles.map}
              >
                <Marker
                  coordinate={{
                    latitude: parseFloat(from ? from : 34.0151) || 34.0151,
                    longitude: parseFloat(to ? to : 71.5249) || 71.5249,
                  }}
                ></Marker>
              </MapView>
              <Card>
                <Card.Title>Notes</Card.Title>
                <Card.Divider />
                <ListItem key={extra_information} bottomDivider>
                  <ListItem.Content>
                    <ListItem.Title>{extra_information}</ListItem.Title>
                  </ListItem.Content>
                </ListItem>
                <View style={styles.buttonGroup}>
                  <TouchableOpacity
                    style={styles.button}
                    onPress={() => this.action("id", "back")}
                  >
                    <Text style={styles.btnSuccess}>GoBack</Text>
                  </TouchableOpacity>

                  {Object.keys(this.state.data.data.job).length > 0 &&
                  this.state.data.data.status != "accept" &&
                  this.state.data.data.status != "completed" ? (
                    <TouchableOpacity
                      style={styles.button}
                      onPress={() => this.action(this.state.job_id, "accept")}
                    >
                      <Text style={styles.btnAccept}>Accept</Text>
                    </TouchableOpacity>
                  ) : Object.keys(this.state.data.data.job).length > 0 &&
                    this.state.data.data.status != "completed" &&
                    this.state.data.data.status != "Pending" ? (
                    <TouchableOpacity
                      style={styles.button}
                      onPress={() =>
                        this.action(this.state.job_id, "completed")
                      }
                    >
                      <Text style={styles.btnAccept}>Completed</Text>
                    </TouchableOpacity>
                  ) : (
                    <Text></Text>
                  )}
                </View>
              </Card>
            </View>
          ) : (
            <Text style={styles.loading}>Loading...</Text>
          )}
        </ScrollView>
      </SafeAreaView>
    );
  }
}
