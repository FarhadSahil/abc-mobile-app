import React, { Component } from "react";
import { View, Text, StyleSheet, Image } from "react-native";

const image = { uri: "https://reactjs.org/logo-og.png" };

export default class Toastr extends Component {
  render() {
    const { message, style } = this.props;
    return (
      <View style={styles.toastr}>
        <Text style={[styles.toastrText, style]}>{message}</Text>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  toastr: {
    padding: 20,
  },
  toastrText: {
    fontSize: 12,
    backgroundColor: "grey",
    padding: 10,
    textAlign: "center",
    color: "white",
    borderRadius: 20,
  },
});
