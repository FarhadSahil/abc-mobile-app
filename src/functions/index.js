import AsyncStorage from "@react-native-async-storage/async-storage";
import { BASE_URL } from "../Config";
export const getJobsList = (setState, user_id, token) => {
  try {
    var OBJECT = {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-type": "application/json",
        Authorization: "Bearer " + token,
      },
      body: JSON.stringify({
        user_id: user_id,
      }),
    };
    fetch(`${BASE_URL}joblist`, OBJECT)
      .then(function (res) {
        return res.json();
      })
      .then((data) => {
        if (data.jobs.length == 0) {
          setState({
            loading: false,
            data: [],
            dataLength: data.length,
          });
          return;
        }
        AsyncStorage.setItem("job_list_count", data.jobs.length.toString());
        const items = [];
        for (var i = 0; i < data.jobs.length; i++) {
          items.push({ item: data.jobs[i] });
          setState({
            loading: true,
            data: items,
            dataLength: data.length,
          });
        }
      });
  } catch (err) {
    // console.log(err);
  }
};

export const getJobListOnEveryRequest = (
  setState,
  user_id,
  token,
  job_list_count
) => {
  const requestOptions = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
    },
    body: JSON.stringify({ user_id: user_id }),
  };
  fetch(`${BASE_URL}countjobs`, requestOptions)
    .then((response) => response.json())
    .then((data) => {
      if (data.jobs.length == 0) {
        AsyncStorage.removeItem("job_list_count");
      }

      if (typeof data.jobs.length !== "undefined" && data.jobs.length > 0) {
        if (
          data.jobs.length > job_list_count &&
          job_list_count != "" &&
          data.jobs.length != ""
        ) {
          AsyncStorage.setItem("lastJob", JSON.stringify(data.last_job));
          setState({
            show_component: true,
            job_object: data.last_job,
            job_list_count: data.jobs.length,
          });
        } else {
          AsyncStorage.setItem("job_list_count", data.jobs.length.toString());
          getJobsList(setState, user_id, token);
        }
      }
    })
    .catch((err) => {
      // console.log();
    });
};

export const _reject = async (setState, job_id, token, user_id, props) => {
  try {
    var $this = this;
    var OBJECT = {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Origin: "",
        Authorization: "Bearer " + token,
      },
      body: JSON.stringify({
        user_id: user_id,
      }),
    };
    fetch(`${BASE_URL}reject-job/${job_id}`, OBJECT)
      .then(function (res) {
        return res.json();
      })
      .then((res) => {
        if (res.success == "success") {
          props.navigation.push("Accepted Job");
        }
      });
  } catch (err) {
    //  console.log(err);
  }
};
export const _detail = (id, props) => {
  props.navigation.navigate("JobDetail", {
    id,
  });
};
