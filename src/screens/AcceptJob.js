import React, { useEffect, useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  SafeAreaView,
  ScrollView,
  StatusBar,
  ActivityIndicator,
} from "react-native";
import { ListItem, Avatar, Card } from "react-native-elements";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { useIsFocused } from "@react-navigation/native";
import Location from "./Location";
import { BASE_URL } from "../Config";
import styles from "../styles/Joblist";
import Moment from "moment";
function AcceptJob({ navigation }) {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [user_id, setUserId] = useState(false);
  const [render, setRender] = useState(false);
  const isFocused = useIsFocused();

  useEffect(() => {
    AsyncStorage.multiGet(["user_id"])
      .then((value) => {
        setUserId(value[0][1]);
      })
      .done();
    let isMounted = true;
    loadAcceptJobData(isMounted);
    return () => {
      isMounted = false;
    }; // use cleanup to toggle value, if unmounted
  }, [user_id, isFocused]);

  function navigate(id) {
    navigation.navigate("JobDetail", {
      id,
    });
  }
  function loadAcceptJobData(isMounted) {
    fetch(`${BASE_URL}accepted-job-list/${user_id}`, {
      method: "GET",
      header: {
        Accept: "application/json",
        "Content-type": "application/json",
      },
    })
      .then((res) => {
        return res.json(); // add conditional check
      })
      .then((res) => {
        if (isMounted) {
          setData(res);
          setLoading(true);
        }
      })
      .catch((err) => {
        // console.log("Error" + err);
      });
  }
  function action(id) {
    navigation.navigate("JobDetail", {
      id,
    });
  }
  // if (loading) {
  //   return (
  //     <SafeAreaView style={styles.container}>
  //       <ScrollView style={styles.scrollView}>
  //         <Location />
  //         <View style={styles.contianer}>
  //           {data != "" &&
  //             data != "undefined" &&
  //             data.map((acceptJob, i) => {
  //               return (
  //                 <TouchableOpacity
  //                   key={i}
  //                   onPress={() => navigate(acceptJob.id)}
  //                 >
  //                   <ListItem bottomDivider>
  //                     <ListItem.Content>
  //                       <ListItem.Title>
  //                         Extran Information:{" "}
  //                         {acceptJob.extra_information.substr(1, 50) ?? ""}
  //                       </ListItem.Title>
  //                       <ListItem.Subtitle>
  //                         Via {acceptJob.via ?? ""}
  //                       </ListItem.Subtitle>
  //                       <ListItem.Subtitle>
  //                         Moving Time {acceptJob.moving_time ?? ""}
  //                       </ListItem.Subtitle>
  //                       <ListItem.Subtitle>
  //                         Firstname {acceptJob.firstname ?? ""}
  //                       </ListItem.Subtitle>
  //                       <ListItem.Subtitle>
  //                         Lastname {acceptJob.lastname ?? ""}
  //                       </ListItem.Subtitle>
  //                     </ListItem.Content>
  //                   </ListItem>
  //                 </TouchableOpacity>
  //               );
  //             })}
  //         </View>
  //       </ScrollView>
  //     </SafeAreaView>
  //   );
  // } else {
  //   return <Text style={styles.loading}>Loading...</Text>;
  // }
  if (loading) {
    Moment.locale("en");
    return (
      <SafeAreaView style={styles.container}>
        <ScrollView style={styles.scrollView}>
          <Location />
          <View style={styles.contianer}>
            {/* {data.map((completed, i) => (
              <TouchableOpacity key={i} onPress={() => navigate(completed.id)}>
                <ListItem bottomDivider>
                  <ListItem.Content>
                    <ListItem.Title>
                      Extran Information:{" "}
                      {completed.extra_information.substr(1, 50)}
                    </ListItem.Title>
                    <ListItem.Subtitle>Via {completed.via}</ListItem.Subtitle>
                    <ListItem.Subtitle>
                      Moving Time {completed.moving_time}
                    </ListItem.Subtitle>
                    <ListItem.Subtitle>
                      Firstname {completed.firstname}
                    </ListItem.Subtitle>
                    <ListItem.Subtitle>
                      Lastname {completed.lastname}
                    </ListItem.Subtitle>
                  </ListItem.Content>
                </ListItem>
              </TouchableOpacity>
            ))} */}
            {data.map((accepted, i) => {
              var upload_job_time = "";
              var current_datetime = new Date();
              var current_day = current_datetime.getDate();
              var server_datetime = accepted.created_at;
              var created_day = Moment(server_datetime).format("D");
              var uploaded_time = Moment(server_datetime).format("LT");
              if (current_day == created_day) {
                upload_job_time = "Today " + uploaded_time;
              } else if ((current_day == created_day) | +1) {
                upload_job_time = "Yesterday " + uploaded_time;
              } else {
                upload_job_time = uploaded_time;
              }
              var uploded_date_year = Moment(server_datetime).format("YY");
              var uploded_date_day = Moment(server_datetime).format("ddd");
              var uploded_date_month = Moment(server_datetime).format("MMM");
              return (
                <TouchableOpacity onPress={() => action(accepted.id)} key={i}>
                  <Card>
                    <Card.Title>{upload_job_time}</Card.Title>
                    <Card.Divider />
                    <View style={styles.cardHeader}>
                      <View style={styles.leftHeader}>
                        <Text style={[styles.txtSize, { fontWeight: "bold" }]}>
                          From: {accepted.from ?? ""}
                        </Text>
                        <Text style={styles.txtSize}>
                          {accepted.vehicle_required ?? ""}
                        </Text>
                        <Text style={styles.txtSize}>
                          {accepted.field_load ?? ""} :
                          {accepted.field_load == "Selfload" ? "Yes" : "No"}
                        </Text>
                        <Text style={[styles.txtSize, { fontWeight: "bold" }]}>
                          Adults: {accepted.text_adults ?? 0}
                        </Text>
                        <Text style={[styles.txtSize, { fontWeight: "bold" }]}>
                          Childrens: {accepted.text_children ?? 0}
                        </Text>
                      </View>

                      <View>
                        <Text style={styles.txtbold}>{uploded_date_day}</Text>
                        <Text style={styles.txtbold}>{uploded_date_year}</Text>
                        <Text style={styles.txtbold}>{uploded_date_month}</Text>
                      </View>
                      <View style={styles.rightHeader}>
                        <Text style={[styles.txtSize, { fontWeight: "bold" }]}>
                          {" "}
                          To: {accepted.to ?? ""}
                        </Text>
                        <Text style={[styles.txtSize, { fontWeight: "bold" }]}>
                          {" "}
                          Move Type
                        </Text>

                        {typeof accepted.job_items !== "undefined" &&
                          accepted.job_items.length > 0 &&
                          accepted.job_items.map((element) => {
                            return (
                              <View key={element.key}>
                                <Text style={styles.txtSize}>
                                  {" "}
                                  {element.item ?? ""}
                                </Text>
                              </View>
                            );
                          })}
                      </View>
                    </View>
                    <Text style={styles.timelimit}>Minimum 2 Hours</Text>
                    <View style={styles.jobinfo}>
                      <Text style={styles.jobinfo_txt}>
                        €{accepted.job_cost ?? ""} AN HOUR
                      </Text>
                      <Text style={styles.jobinfo_txt}>
                        Name:{accepted.fullname ?? ""}
                      </Text>
                      <Text style={styles.jobinfo_txt}>
                        {accepted.contact_no ?? ""}
                      </Text>
                    </View>
                    <View style={styles.buttonGroup}>
                      <TouchableOpacity
                        style={[
                          styles.button,
                          {
                            justifyContent: "center",
                            flex: 1,
                            alignItems: "center",
                          },
                        ]}
                        onPress={() => navigation.navigate("Job List")}
                      >
                        <Text style={styles.btnDetail}>Continue</Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={[
                          styles.button,
                          {
                            justifyContent: "center",
                            flex: 1,
                            alignItems: "center",
                          },
                        ]}
                        onPress={() => action(accepted.id)}
                      >
                        <Text style={styles.btnDetail}>Detail</Text>
                      </TouchableOpacity>
                    </View>
                  </Card>
                </TouchableOpacity>
              );
            })}
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  } else {
    return <ActivityIndicator size="small" color="#0000ff" />;
  }
}
// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     paddingTop: StatusBar.currentHeight,
//   },
//   loading: {
//     fontSize: 12,
//     flex: 1,
//     justifyContent: "center",
//     textAlign: "center",
//     width: 100 + "%",
//     height: 30,
//     lineHeight: 30,
//     color: "white",
//     borderRadius: 3,
//     marginTop: 10,
//     color: "red",
//   },
// });

export default AcceptJob;
