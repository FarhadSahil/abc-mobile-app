import React, { Component } from "react";

import {
  StyleSheet,
  TextInput,
  View,
  Alert,
  Button,
  Text,
  Image,
  TouchableOpacity,
  StatusBar,
  SafeAreaView,
  ScrollView,
} from "react-native";
import FormData from "form-data";
import { ListItem, Avatar } from "react-native-elements";

import { AssetsSelector } from "expo-images-picker";
import * as ImagePicker from "expo-image-picker";

import { Ionicons } from "@expo/vector-icons";

import styles from "../styles/Login";
import { Camera } from "expo-camera";
import CameraPrview from "./CameraPrview";
import { BASE_URL } from "../Config";
let camera;
export default class AddExtraItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      amount: 0,
      item: "",
      cameraStart: false,
      btnCameraShow: false,
      flashMode: "off",
      galleryPhoto: [],
      success_msg: "",
      error_msg: "",
    };
  }
  componentDidMount() {
    // this.getPermissionAsync();
  }
  async __startCamera() {
    const { status } = await Camera.requestPermissionsAsync();
    if (status === "granted") {
      this.setState({
        cameraStart: true,
        btnCameraShow: true,
        cameraPhoto: "",
        cameraType: Camera.Constants.Type.back,
      });
    } else {
      console.log("access denied");
    }
  }
  async __takePicture() {
    if (!camera) return;
    const cameraPhoto = await camera.takePictureAsync();
    this.setState({ cameraPhoto, flashMode: "off" });
    console.log(cameraPhoto);
  }

  __handleFlashMode() {
    if (this.state.flashMode === "on") {
      this.setState({ flashMode: "off" });
    } else if (this.state.flashMode === "off") {
      this.setState({ flashMode: "on" });
    } else {
      this.setState({ flashMode: "auto" });
      // setFlashMode("auto");
    }
  }

  __switchCamera() {
    if (this.state.cameraType === "back") {
      this.setState({ cameraType: "front" });
    } else {
      this.setState({ cameraType: "back" });
    }
  }

  async getPermissionAsync() {
    const { status } = await ImagePicker.requestMediaLibraryPermissionsAsync();
    if (status != "granted") {
      alert("Please granted the permission");
    }
  }
  async __attachPictures() {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      // allowsEditing: true,
      // allowsMultipleSelection: true,
      aspect: [4, 3],
      quality: 1,
    });

    if (!result.cancelled) {
      var joined = this.state.galleryPhoto.concat(result);
      this.setState({ galleryPhoto: joined });
    }
  }
  async submit() {
    try {
      const data = new FormData();
      const { amount, item, cameraPhoto, galleryPhoto } = this.state;
      console.log("amount", amount);
      console.log("item", item);
      if (amount == 0 && item == "" && !cameraPhoto) {
        this.setState({ error_msg: "All fields are required" });
        return;
      }
      const { job_id } = this.props.route.params;

      if (cameraPhoto) {
        data.append("image", {
          uri: cameraPhoto.uri, // your file path string
          name: "IMG_20180822152132_802.jpg",
          type: "image/jpg",
        });
      } else {
        this.state.galleryPhoto.forEach((item, i) => {
          data.append("images[]", {
            uri: item.uri,
            type: "image/jpeg",
            name: item.filename || `filename${i}.jpg`,
          });
        });
      }
      // for (var pair of data.entries()) {
      //   console.log(pair[0] + ", " + pair[1]);
      // }
      data.append("job_id", job_id);
      data.append("amount", amount);
      data.append("item", item);
      let res = await fetch(`${BASE_URL}add-extra-item`, {
        method: "post",
        body: data,
        headers: {
          Accept: "application/json",
          "Content-Type": "multipart/form-data; ",
        },
      });
      let responseJson = await res.json();
      // console.log(responseJson);
      if (responseJson[0] == "success") {
        // console.log("i am fine");
        this.setState({
          amount: "",
          item: "",
          galleryPhoto: [],
          cameraPhoto: "",
          success_msg: "Item has been added",
          cameraStart: false,
          btnCameraShow: false,
          error_msg: "",
        });
        this.item.clear();
        this.amount.clear();
      }
    } catch (e) {
      // console.log("Error", e);
    }
    // console.log("cameraPhoto", this.state.cameraPhoto);
  }
  goBack() {}
  remove(key) {
    var galleryPhoto_array = this.state.galleryPhoto;
    delete galleryPhoto_array[key];
    this.setState({ galleryPhoto: galleryPhoto_array });
  }
  render() {
    return (
      // <View>
      //   <AssetsSelector
      //     options={{
      //       /* Add only when u want to Manipulate Assets.
      //     manipulate: {
      //     width: 512,
      //     compress: 0.7,
      //     base64: false,
      //     saveTo: 'jpeg',
      //   },*/
      //       assetsType: ["cameraPhoto", "video"],
      //       maxSelections: 5,
      //       margin: 3,
      //       portraitCols: 4,
      //       landscapeCols: 5,
      //       widgetWidth: 100,
      //       widgetBgColor: "green",
      //       selectedBgColor: "red",
      //       spinnerColor: "blue",
      //       videoIcon: {
      //         Component: Ionicons,
      //         iconName: "ios-videocam",
      //         color: "white",
      //         size: 20,
      //       },
      //       selectedIcon: {
      //         Component: Ionicons,
      //         iconName: "ios-checkmark-circle-outline",
      //         color: "white",
      //         bg: "white",
      //         size: 20,
      //       },
      //       defaultTopNavigator: {
      //         selectedText: "Selected",
      //         continueText: "Finish",
      //         goBackText: "Back",
      //         midTextColor: "red",
      //         buttonStyle: styles.btnTextColor,
      //         textStyle: styles.btnTextColor,
      //         backFunction: this.goBack.bind(this),
      //         doneFunction: (data) => onDone(data),
      //       },
      //       // noAssets: CustomNoAssetsComponent,
      //     }}
      //   />
      // </View>
      <ScrollView>
        <View>
          {this.state.galleryPhoto.map((l, i) => (
            <ListItem key={i} bottomDivider>
              <Avatar source={{ uri: l.uri }} />
              <ListItem.Title>
                <TouchableOpacity
                  style={styles.removeButton}
                  onPress={() => this.remove(i)}
                >
                  <Text>Remove</Text>
                </TouchableOpacity>
              </ListItem.Title>
            </ListItem>
          ))}
        </View>
        <View>
          {this.state.btnCameraShow && !this.state.cameraPhoto ? (
            <View
              style={{
                position: "absolute",
                top: 190,
                flexDirection: "row",
                flex: 1,
                width: "100%",
                padding: 20,
                justifyContent: "space-between",
              }}
            >
              <View
                style={{
                  alignSelf: "center",
                  flex: 1,
                  flexDirection: "row",
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                <TouchableOpacity
                  onPress={this.__handleFlashMode.bind(this)}
                  style={{
                    width: 70,
                    height: 70,
                    bottom: 0,
                    borderRadius: 50,
                    zIndex: 1,
                    marginLeft: 20,
                    backgroundColor:
                      this.state.flashMode == "off" ? "#f15a36" : "white",
                  }}
                >
                  <Text
                    style={{
                      color:
                        this.state.flashMode == "off" ? "white" : "#f15a36",
                      fontSize: 12,
                      textAlign: "center",
                      flex: 1,
                      marginTop: 23,
                    }}
                  >
                    Flash
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={this.__takePicture.bind(this)}
                  style={{
                    width: 70,
                    height: 70,
                    bottom: 0,
                    borderRadius: 50,
                    zIndex: 1,
                    marginLeft: 10,
                    backgroundColor: "white",
                  }}
                />

                <TouchableOpacity
                  onPress={this.__switchCamera.bind(this)}
                  style={{
                    width: 70,
                    height: 70,
                    bottom: 0,
                    borderRadius: 50,
                    zIndex: 1,
                    marginLeft: 10,
                    backgroundColor: "white",
                  }}
                >
                  <Text
                    style={{
                      color: "#f15a36",
                      fontSize: 12,
                      textAlign: "center",
                      flex: 1,
                      marginTop: 23,
                    }}
                  >
                    Camera
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          ) : (
            <Text></Text>
          )}
          {this.state.cameraStart && !this.state.cameraPhoto ? (
            <View>
              <Camera
                type={this.state.cameraType}
                flashMode={this.state.flashMode}
                style={styles.camera}
                ref={(r) => {
                  camera = r;
                }}
                style={{ height: 300 }}
              ></Camera>
            </View>
          ) : this.state.cameraPhoto ? (
            <CameraPrview cameraPhoto={this.state.cameraPhoto} />
          ) : (
            <Text></Text>
          )}

          <Text
            style={{
              fontSize: 30,
              textAlign: "center",
              marginTop: 70,
              fontWeight: "bold",
            }}
          >
            Add Extra Item
          </Text>
          {this.state.success_msg ? (
            <Text
              style={{
                fontSize: 15,
                textAlign: "center",
                marginTop: 70,
                fontWeight: "bold",
                color: "#42ba96",
              }}
            >
              {this.state.success_msg}
            </Text>
          ) : (
            <Text></Text>
          )}
          {this.state.error_msg ? (
            <Text
              style={{
                fontSize: 15,
                textAlign: "center",
                marginTop: 70,
                fontWeight: "bold",
                color: "#df4759",
              }}
            >
              {this.state.error_msg}
            </Text>
          ) : (
            <Text></Text>
          )}
          {/* <Image source={require("./background.jpg")} style={styles.img} /> */}
          <View style={[styles.mainContent, { marginTop: 20 }]}>
            <TextInput
              onChangeText={(itm) => this.setState({ item: itm })}
              style={styles.loginInput}
              placeholder="Item"
              // secureTextEntry={true}
              autoCapitalize="none"
              ref={(input) => {
                this.item = input;
              }}
            />
            <TextInput
              onChangeText={(amt) => this.setState({ amount: amt })}
              style={styles.loginInput}
              placeholder="Amount"
              keyboardType="numeric"
              autoCapitalize="none"
              ref={(input) => {
                this.amount = input;
              }}
            />

            <TouchableOpacity
              style={[styles.loginButton, { paddingBottom: 20 }]}
              onPress={this.__attachPictures.bind(this)}
            >
              <Text style={styles.btnTextColor}>Attach Pictures</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[styles.loginButton, { paddingBottom: 20 }]}
              onPress={this.__startCamera.bind(this)}
            >
              <Text style={styles.btnTextColor}>Take Picture</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[styles.loginButton, { paddingBottom: 20, marginTop: 20 }]}
              onPress={this.submit.bind(this)}
            >
              <Text style={styles.btnTextColor}>Submit</Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    );
  }
}
// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     flexDirection: "column",
//     justifyContent: "center",
//     alignItems: "center",
//   },
//   img: {
//     width: 100,
//     height: 100,
//     borderRadius: 100 / 2,
//   },
//   heading: {
//     fontSize: 30,
//     textAlign: "center",
//     marginTop: 70,
//     fontWeight: "bold",
//   },
// });
