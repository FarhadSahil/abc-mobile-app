import React, { Component } from "react";
import { View, Text, Image, StyleSheet } from "react-native";
export default class CameraPrview extends Component {
  constructor({ props }) {
    super(props);
  }
  render() {
    return (
      <View
        style={{
          backgroundColor: "transparent",
          flex: 1,
          width: "90%",
          height: 250,
          marginTop: 20,
        }}
      >
        <Image
          source={{ uri: this.props.cameraPhoto && this.props.cameraPhoto.uri }}
          style={styles.img}
        />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  img: {
    flex: 1,
    flexDirection: "row",
    marginLeft: 40,
  },
});
