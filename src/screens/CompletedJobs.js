import React, { useEffect, useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  SafeAreaView,
  ScrollView,
  ActivityIndicator,
  StatusBar,
} from "react-native";

import { ListItem, Avatar, Card } from "react-native-elements";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { useIsFocused } from "@react-navigation/native";
import Location from "./Location";
import { BASE_URL } from "../Config";
import styles from "../styles/Joblist";
import Moment from "moment";

function CompletedJobs({ navigation }) {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [user_id, setUserId] = useState(false);
  const [render, setRender] = useState(false);
  const isFocused = useIsFocused();

  useEffect(() => {
    let isMounted = true;
    AsyncStorage.multiGet(["user_id"])
      .then((value) => {
        setUserId(value[0][1]);
      })
      .done();
    loadAcceptJobData(isMounted);
    return () => {
      isMounted = false;
    };
  }, [user_id, isFocused]);

  function navigate(id) {
    navigation.navigate("JobDetail", {
      id,
    });
  }
  function loadAcceptJobData(isMounted) {
    fetch(`${BASE_URL}completed-job-list/${user_id}`, {
      method: "GET",
      header: {
        Accept: "application/json",
        "Content-type": "application/json",
      },
    })
      .then((res) => {
        return res.json(); // add conditional check
      })
      .then((res) => {
        if (isMounted) {
          setData(res);
          setLoading(true);
        }
      })
      .catch((err) => {
        //   console.log("Error" + err);
      });
  }
  function action(id, status) {
    navigation.navigate("JobDetail", {
      id,
    });
  }
  if (loading) {
    Moment.locale("en");
    return (
      <SafeAreaView style={styles.container}>
        <ScrollView style={styles.scrollView}>
          <Location />
          <View style={styles.contianer}>
            {data.map((completed, i) => {
              var upload_job_time = "";
              var current_datetime = new Date();
              var current_day = current_datetime.getDate();
              var server_datetime = completed.created_at;
              var created_day = Moment(server_datetime).format("D");
              var uploaded_time = Moment(server_datetime).format("LT");
              if (current_day == created_day) {
                upload_job_time = "Today " + uploaded_time;
              } else if ((current_day == created_day) | +1) {
                upload_job_time = "Yesterday " + uploaded_time;
              } else {
                upload_job_time = uploaded_time;
              }
              var uploded_date_year = Moment(server_datetime).format("YY");
              var uploded_date_day = Moment(server_datetime).format("ddd");
              var uploded_date_month = Moment(server_datetime).format("MMM");
              return (
                <TouchableOpacity
                  onPress={() => action(completed.id, "continue")}
                  key={completed.id}
                >
                  <Card>
                    <Card.Title>{upload_job_time}</Card.Title>
                    <Card.Divider />
                    <View style={styles.cardHeader}>
                      <View style={styles.leftHeader}>
                        <Text style={[styles.txtSize, { fontWeight: "bold" }]}>
                          From: {completed.from ?? ""}
                        </Text>
                        <Text style={styles.txtSize}>
                          {completed.vehicle_required ?? ""}
                        </Text>
                        <Text style={styles.txtSize}>
                          {completed.field_load ?? ""} :
                          {completed.field_load == "Selfload" ? "Yes" : "No"}
                        </Text>
                        <Text style={[styles.txtSize, { fontWeight: "bold" }]}>
                          Adults: {completed.text_adults ?? 0}
                        </Text>
                        <Text style={[styles.txtSize, { fontWeight: "bold" }]}>
                          Childrens: {completed.text_children ?? 0}
                        </Text>
                      </View>

                      <View>
                        <Text style={styles.txtbold}>{uploded_date_day}</Text>
                        <Text style={styles.txtbold}>{uploded_date_year}</Text>
                        <Text style={styles.txtbold}>{uploded_date_month}</Text>
                      </View>
                      <View style={styles.rightHeader}>
                        <Text style={[styles.txtSize, { fontWeight: "bold" }]}>
                          {" "}
                          To: {completed.to ?? ""}
                        </Text>
                        <Text style={[styles.txtSize, { fontWeight: "bold" }]}>
                          {" "}
                          Move Type
                        </Text>
                        {typeof completed.job_items !== "undefined" &&
                          completed.job_items.length > 0 &&
                          completed.job_items.map((element) => {
                            return (
                              <View key={element.key}>
                                <Text style={styles.txtSize}>
                                  {" "}
                                  {element.item ?? ""}
                                </Text>
                              </View>
                            );
                          })}
                      </View>
                    </View>
                    <Text style={styles.timelimit}>Minimum 2 Hours</Text>
                    <View style={styles.jobinfo}>
                      <Text style={styles.jobinfo_txt}>
                        €{completed.job_cost ?? ""} AN HOUR
                      </Text>
                      <Text style={styles.jobinfo_txt}>
                        Name:{completed.fullname ?? ""}
                      </Text>
                      <Text style={styles.jobinfo_txt}>
                        {completed.contact_no ?? ""}
                      </Text>
                    </View>
                    <View style={styles.buttonGroup}>
                      {completed.job_status != "completed" ? (
                        <TouchableOpacity
                          style={[
                            styles.button,
                            {
                              justifyContent: "center",
                              flex: 1,
                              alignItems: "center",
                            },
                          ]}
                          onPress={() => action("id", "continue")}
                        >
                          <Text style={styles.btnDetail}>Continue</Text>
                        </TouchableOpacity>
                      ) : (
                        <TouchableOpacity
                          style={[
                            styles.button,
                            {
                              flex: 1,
                              alignSelf: "stretch",
                            },
                          ]}
                        >
                          <Text style={styles.btnDetail}>Job Completed</Text>
                        </TouchableOpacity>
                      )}
                    </View>
                  </Card>
                </TouchableOpacity>
              );
            })}
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  } else {
    return <ActivityIndicator size="small" color="#0000ff" />;
  }
}

export default CompletedJobs;
