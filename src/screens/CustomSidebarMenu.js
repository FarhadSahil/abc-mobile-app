// Custom Navigation Drawer / Sidebar with Image and Icon in Menu Options
// https://aboutreact.com/custom-navigation-drawer-sidebar-with-image-and-icon-in-menu-options/

import React from "react";
import {
  SafeAreaView,
  View,
  StyleSheet,
  Image,
  Text,
  Linking,
} from "react-native";

import {
  DrawerContentScrollView,
  DrawerItemList,
  DrawerItem,
} from "@react-navigation/drawer";

const CustomSidebarMenu = (props) => {
  return (
    <SafeAreaView style={{ flex: 1 }}>
      {/*Top Large Image */}

      <View style={styles.imageTextSideBySide}>
        <Image
          source={{
            uri: "https://leaderbord-552b1.firebaseapp.com/AdminLTE%202%20_%20User%20Profile_files/mount-carmel-logo.png",
          }}
          style={{ width: 70, height: 70, borderRadius: 70 }}
        />
        <Text style={styles.rightText}>Rating #3.1</Text>
      </View>
      <View>
        <Text style={styles.leftText}>ID:R-1521</Text>
      </View>
      <DrawerContentScrollView {...props}>
        <DrawerItemList {...props} />
      </DrawerContentScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  imageTextSideBySide: {
    flexDirection: "row",
    textAlign: "left",
    fontSize: 15,
    marginTop: 20,
  },
  rightText: {
    alignSelf: "center",
    fontSize: 20,
    fontWeight: "bold",
    marginLeft: 10,
    color: "white",
  },
  leftText: {
    alignSelf: "center",
    fontSize: 20,
    fontWeight: "bold",
    color: "white",
    marginRight: 40,
    marginTop: 10,
  },
  image: {
    width: 50,
    height: 50,
  },
  sideMenuProfileIcon: {
    resizeMode: "center",
    width: 100,
    height: 100,
    borderRadius: 100 / 2,
    alignSelf: "center",
  },
  iconStyle: {
    width: 15,
    height: 15,
    marginHorizontal: 5,
  },
  customItem: {
    padding: 16,
    flexDirection: "row",
    alignItems: "center",
  },
});

export default CustomSidebarMenu;
