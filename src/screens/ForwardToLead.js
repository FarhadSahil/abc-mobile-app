import React, { Component, useEffect, useState } from "react";
import {
  View,
  Text,
  ScrollView,
  SafeAreaView,
  TouchableOpacity,
  BackHandler,
  DeviceEventEmitter,
} from "react-native";
import { Card, ListItem, ThemeContext } from "react-native-elements";
import AsyncStorage from "@react-native-async-storage/async-storage";
import styles from "../styles/Joblist";
import Moment from "moment";

import { BASE_URL } from "../Config";
export default class ForwardtoLead extends Component {
  constructor() {
    super();
    this.state = {
      user_id: "",
      token: "",
      loading: false,
      data: [],
      params: "",
    };
  }
  componentDidMount() {
    this.exploreAsyncStorageForUserIdAndToken();
  }

  exploreAsyncStorageForUserIdAndToken = () => {
    AsyncStorage.multiGet(["token", "user_id"])
      .then((value) => {
        this.setState({
          params: value,
          loading: true,
        });
        this.forwardToLeadJobs(this.state.params);
      })
      .done();
  };
  forwardToLeadJobs = (params) => {
    fetch(`${BASE_URL}forward-lead-jobs/${params[1][1]}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((response) => response.json())
      .then((res) => {
        console.log("farnad", res);
        this.setState({ data: res });
      })
      .catch((error) => {
        console.error(error);
      });
  };
  action = (id, action) => {
    if (action == "reject") {
      AsyncStorage.multiGet(["token", "user_id"])
        .then((value) => {
          this.setState({
            params: value,
            loading: true,
          });

          try {
            var $this = this;
            var OBJECT = {
              method: "POST",
              headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                Origin: "",
                Authorization: "Bearer " + this.state.token,
              },
              body: JSON.stringify({
                user_id: value[1][1],
                job_id: id,
              }),
            };
            //   console.log("body", OBJECT.body);
            fetch(`${BASE_URL}reject-forward-to-lead-job`, OBJECT)
              .then(function (res) {
                return res.json();
              })
              .then((res) => {
                // console.log(res);
                if (res.success == "success") {
                  this.props.navigation.navigate("Accept jobs");
                }
              });
          } catch (err) {
            // console.log(err);
          }
        })
        .done();
    }
    if (action == "detail") {
      this.props.navigation.navigate("JobDetail", {
        id: id,
        datatype: "forwardtolead",
      });
    }
  };
  render() {
    Moment.locale("en");

    return (
      <SafeAreaView style={styles.container}>
        <ScrollView style={styles.scrollView}>
          {this.state.loading ? (
            <View>
              {this.state.data.map((data, index) => {
                if (data.job != null) {
                  var upload_job_time = "";
                  var current_datetime = new Date();
                  var current_day = current_datetime.getDate();
                  var server_datetime = data.job.created_at;
                  var created_day = Moment(server_datetime).format("D");
                  var uploaded_time = Moment(server_datetime).format("LT");
                  if (current_day == created_day) {
                    upload_job_time = "Today " + uploaded_time;
                  } else if ((current_day == created_day) | +1) {
                    upload_job_time = "Yesterday " + uploaded_time;
                  } else {
                    upload_job_time = uploaded_time;
                  }
                  var uploded_date_year = Moment(server_datetime).format("YY");
                  var uploded_date_day = Moment(server_datetime).format("ddd");
                  var uploded_date_month =
                    Moment(server_datetime).format("MMM");
                }
                if (data.job !== null) {
                  return (
                    <TouchableOpacity
                      onPress={() => this.action(data.job.id, "detail")}
                      key={index}
                    >
                      <Card>
                        <Card.Title>{upload_job_time}</Card.Title>
                        <Card.Divider />
                        <View style={styles.cardHeader}>
                          <Text style={styles.txtSize}>
                            From: {data.job.from ?? ""}
                          </Text>
                          <View>
                            <Text style={styles.txtbold}>
                              {uploded_date_day}
                            </Text>
                            <Text style={styles.txtbold}>
                              {uploded_date_year}
                            </Text>
                            <Text style={styles.txtbold}>
                              {uploded_date_month}
                            </Text>
                          </View>
                          <Text style={styles.txtSize}>
                            {" "}
                            To: {data.job.to ?? ""}
                          </Text>
                        </View>
                        <Text style={styles.timelimit}>Minimum 2 Hours</Text>
                        <View style={styles.jobinfo}>
                          <Text style={styles.jobinfo_txt}>
                            ${data.job.job_cost ?? ""} AN HOUR
                          </Text>
                          <Text style={styles.jobinfo_txt}>
                            Name:{data.job.firstname ?? ""}
                          </Text>
                          <Text style={styles.jobinfo_txt}>34234322</Text>
                        </View>
                        <View style={styles.buttonGroup}>
                          {data.status != "reject" ? (
                            <TouchableOpacity
                              style={styles.button}
                              onPress={() => this.action(data.job.id, "reject")}
                            >
                              <Text style={styles.btnReject}>Reject</Text>
                            </TouchableOpacity>
                          ) : (
                            <TouchableOpacity style={styles.button}>
                              <Text style={styles.btnReject}>
                                Already Rejected
                              </Text>
                            </TouchableOpacity>
                          )}
                          <TouchableOpacity
                            style={styles.button}
                            onPress={() => this.action(data.job.id, "detail")}
                          >
                            <Text style={styles.btnDetail}>DETAIL</Text>
                          </TouchableOpacity>
                        </View>
                      </Card>
                    </TouchableOpacity>
                  );
                } else {
                  return <Text></Text>;
                }
              })}
            </View>
          ) : (
            <Text style={styles.loading}>Loading...</Text>
          )}
        </ScrollView>
      </SafeAreaView>
    );
  }
}
