import * as React from "react";
import { createDrawerNavigator } from "@react-navigation/drawer";
import JobList from "./JobList";
import AcceptJob from "./AcceptJob";
import CompletedJobs from "./CompletedJobs";
import onJob from "./onJob";
import History from "./History";
import Message from "./Message";
import ViewScreen from "./ViewScreen";
import Support from "./Support";
import Logout from "./Logout";
import CustomSidebarMenu from "./CustomSidebarMenu";
import ForwardtoLead from "./ForwardToLead";
import UserLocationTracker from "./UserLocationTracker";
import AddExtraItem from "./AddExtraItem";

const Drawer = createDrawerNavigator();

export default function Home(props) {
  return (
    <Drawer.Navigator
      initialRouteName="Accept jobs"
      drawerContent={(props) => <CustomSidebarMenu {...props} />}
      drawerStyle={{
        backgroundColor: "#f4511e",
        width: 240,
        color: "white",
      }}
      drawerContentOptions={{
        activeTintColor: "black",
        activeBackgroundColor: "#FCE3CF",
        inactiveTintColor: "black",
        inactiveBackgroundColor: "white",
        labelStyle: {
          marginLeft: 5,
        },
      }}
    >
      <Drawer.Screen name="ForwardtoLead" component={ForwardtoLead} />
      <Drawer.Screen name="Job List" component={JobList} />
      <Drawer.Screen name="Accept jobs" component={AcceptJob} />
      <Drawer.Screen name="Completd Job list" component={CompletedJobs} />
      <Drawer.Screen name="History" component={History} />
      <Drawer.Screen name="View" component={ViewScreen} />
      <Drawer.Screen name="Message" component={Message} />
      <Drawer.Screen name="Support" component={Support} />
      <Drawer.Screen name="On Job" component={onJob} />
      <Drawer.Screen name="AddExtraItem" component={AddExtraItem} />
      <Drawer.Screen
        name="UserLocationTracker"
        component={UserLocationTracker}
      />
      <Drawer.Screen name="Logout" component={Logout} />
    </Drawer.Navigator>
  );
}
