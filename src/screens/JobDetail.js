import React, { Component } from "react";
import {
  Dimensions,
  StyleSheet,
  Linking,
  Platform,
  Alert,
  ActivityIndicator,
} from "react-native";
import MapView, { Marker } from "react-native-maps";
import MapViewDirections from "react-native-maps-directions";

import { Snackbar } from "react-native-paper";
// npm start --reset-cache

import {
  View,
  Text,
  ScrollView,
  SafeAreaView,
  TouchableOpacity,
} from "react-native";
import {
  Card,
  ListItem,
  Button,
  Icon,
  Avatar,
  ButtonGroup,
} from "react-native-elements";
import { BASE_URL } from "../Config";
import styles from "../styles/JobDetail";

// LogBox.ignoreLogs(['VirtualizedLists should never be nested']);

import AsyncStorage from "@react-native-async-storage/async-storage";
const INJECTEDJAVASCRIPT = `const meta = document.createElement('meta'); meta.setAttribute('content', 'width=device-width, initial-scale=0.5, maximum-scale=0.5, user-scalable=0'); meta.setAttribute('name', 'viewport'); document.getElementsByTagName('head')[0].appendChild(meta); `;
const { width, height } = Dimensions.get("window");
const ASPECT_RATIO = width / height;
const LATITUDE = 37.771707;
const LONGITUDE = -122.4053769;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

const GOOGLE_MAPS_APIKEY = "AIzaSyCGvbUdrsJOZuuHbD0q_J-BWkO5c8xhl40";
export default class JobDetail extends Component {
  static navigationOptions = {
    title: "Job Detail",
    headerLeft: null,
    headerTitleStyle: {
      textAlign: "left",
      flex: 1,
    },
  };

  constructor(props) {
    super(props);
    this.state = {
      coordinates: [
        // {
        //   latitude: 53.544652717917906,
        //   longitude: -2.1128581133334388,
        // },
        // {
        //   latitude: 53.48809988202768,
        //   longitude: -2.235832044857433,
        // },
      ],
      user_email: "",
      list: [],
      action: "",
      changedVal: "Edit",
      data: [],
      loading: false,
      selectedIndex: 2,
      job: "",
      item: "",
      disabled: false,
      sanckbarVisible: false,
      job_id: "",
      countforWardToLeadAcceptStatus: "",
    };
    AsyncStorage.multiGet(["token", "user_id"])
      .then((value) => {
        this.setState({ token: value[0][1], user_id: value[1][1] });
        const job_id = this.props.route.params.id;

        const datatype = this.props.route.params.datatype ?? "noaction";
        this.getJobsList(job_id, datatype);
      })
      .done();
    this.mapView;
  }
  getJobsList(job_id, datatype = null) {
    try {
      var $this = this;
      var OBJECT = {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Origin: "",
          Authorization: "Bearer " + this.state.token,
        },
        body: JSON.stringify({
          user_id: this.state.user_id,
          datatype,
        }),
      };
      fetch(`${BASE_URL}job/${job_id}`, OBJECT)
        .then(function (res) {
          return res.json();
        })
        .then((data) => {
          var coordinates = [...this.state.coordinates];
          let coords = [];
          coords[0] = {
            latitude: Number(data.origin_lat),
            longitude: Number(data.origin_lat),
          };
          coords[1] = {
            latitude: Number(data.dest_lat),
            longitude: Number(data.dest_long),
          };
          //console.log("coords", coords);
          //console.log("coordinates", coordinates);
          this.setState({
            loading: true,
            data: data,
            job_id,
            coordinates: coords,
          });
        });
    } catch (err) {
      //console.log(err);
    }
  }

  action = (id = null, action) => {
    if (action == "naviagateToJob") {
      this.onJob(id);
    }
    if (action == "accept") {
      this.acceptJob(id);
    }
    if (action == "add_extra_item") {
      this.props.navigation.navigate("AddExtraItem", { job_id: id });
    }
    if (action == "completed") {
      this.completeJob(id);
    }
    if (action == "acceptfrdtolead") {
      this.acceptForwardToLeadJob(id);
    }
  };

  onJob = (job_id) => {
    fetch(`${BASE_URL}change-job-status/${job_id}`)
      .then((response) => {
        return response.json();
      })
      .then((res) => {
        if (res.success) {
          var from = res.job.from;
          Linking.openURL(`google.navigation:q=${from}`);
          // var lat = res.job.dest_lat;
          // var long = res.job.dest_long;
          // Linking.openURL(`google.navigation:q=${lat}, ${long}`);
          // this.props.navigation.navigate("LiveLocationTracker", {
          //   job_id: job_id,
          // });
        }
      });
  };
  acceptForwardToLeadJob = (job_id) => {
    const data = {
      job_id,
      user_id: this.state.user_id,
    };
    fetch(`${BASE_URL}accept-forward-to-lead-job`, {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    })
      .then((response) => response.json())
      .then((res) => {
        if (res.token) {
          AsyncStorage.setItem("token", res.token);
          AsyncStorage.setItem("user_id", JSON.stringify(res.user.id));
          props.navigation.navigate("Accepted Job", {
            screen: "Job List",
          });
        } else {
          setloginResponse(res.error);
        }
      })
      .catch((error) => {
        console.error(error);
      });
  };

  _goBack = () => {
    this.props.navigation.navigate("Job List");
  };

  completeJob = (id) => {
    try {
      var $this = this;
      var OBJECT = {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Origin: "",
          Authorization: "Bearer " + this.state.token,
        },
        body: JSON.stringify({
          user_id: this.state.user_id,
        }),
      };
      fetch(`${BASE_URL}compeleted-job/${id}`, OBJECT)
        .then(function (res) {
          return res.json();
        })
        .then((res) => {
          //console.log(res);
          if (res.success) {
            this.setState({ sanckbarVisible: true });
            setTimeout(() => {
              this.setState({ sanckbarVisible: false });
              this.props.navigation.navigate("Accepted Job", {
                screen: "Completd Job list",
              });
            }, 5000);
          }
          // this.setState({ loading: true, data: data });
        });
    } catch (err) {
      //console.log(err);
    }
  };
  async acceptJob(id) {
    try {
      var $this = this;
      var OBJECT = {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Origin: "",
          Authorization: "Bearer " + this.state.token,
        },
        body: JSON.stringify({
          user_id: this.state.user_id,
        }),
      };
      fetch(`${BASE_URL}accept-job/${id}`, OBJECT)
        .then(function (res) {
          return res.json();
        })
        .then((res) => {
          // //console.log("hi" + JSON.stringify(res));
          if (res.success == "success") {
            this.props.navigation.navigate("Accept jobs");
          }
        });
    } catch (err) {
      //console.log(err);
    }
  }
  checkForForwardToleadJobAcceptance = () => {
    fetch(`${BASE_URL}checkfor-job-forfield-status/${this.state.job_id}`)
      .then((response) => {
        return response.json();
      })
      .then((res) => {
        this.setState({ countforWardToLeadAcceptStatus: res.count });
      });
  };
  onMapPress = () => {};
  render() {
    var from, to, extra_information, job_id;
    if (this.state.loading && this.state.data.from != undefined) {
      from = this.state.data.from;
      to = this.state.data.to;
      extra_information = this.state.data.extra_information;
      job_id = this.state.data.id;
      this.checkForForwardToleadJobAcceptance();
    }
    return (
      <SafeAreaView style={styles.container}>
        <ScrollView style={styles.scrollView}>
          <Snackbar
            visible={this.state.sanckbarVisible}
            onDismiss={() => this.setState({ visible: false })}
          >
            Email has been sent.
          </Snackbar>
          {this.state.loading && this.state.coordinates.length >= 2 ? (
            <View>
              <MapView
                initialRegion={{
                  latitude: LATITUDE,
                  longitude: LONGITUDE,
                  latitudeDelta: LATITUDE_DELTA,
                  longitudeDelta: LONGITUDE_DELTA,
                }}
                style={{
                  alignSelf: "stretch",
                  flex: 1,
                  height: 300,
                  marginTop: 20,
                }}
                ref={(c) => (this.mapView = c)}
                onPress={this.onMapPress}
              >
                {this.state.coordinates.map((coordinate, index) => (
                  <MapView.Marker
                    key={`coordinate_${index}`}
                    coordinate={coordinate}
                  />
                ))}
                {this.state.coordinates.length >= 0 && (
                  <MapViewDirections
                    origin={{
                      latitude: this.state.coordinates[0].latitude,
                      longitude: this.state.coordinates[0].longitude,
                    }}
                    destination={{
                      latitude: this.state.coordinates[1].latitude,
                      longitude: this.state.coordinates[1].longitude,
                    }}
                    apikey={GOOGLE_MAPS_APIKEY}
                    strokeWidth={3}
                    strokeColor="hotpink"
                    optimizeWaypoints={true}
                    onStart={(params) => {}}
                    onReady={(result) => {
                      this.mapView.fitToCoordinates(result.coordinates, {
                        edgePadding: {
                          right: width / 20,
                          bottom: height / 20,
                          left: width / 20,
                          top: height / 20,
                        },
                      });
                    }}
                    onError={(errorMessage) => {}}
                  />
                )}
              </MapView>

              <Card>
                <Card.Title>{extra_information}</Card.Title>
                <Card.Divider />
                <ListItem key={extra_information} bottomDivider>
                  <ListItem.Content>
                    <ListItem.Title>{extra_information}</ListItem.Title>
                  </ListItem.Content>
                </ListItem>
                <View style={styles.buttonGroup}>
                  <TouchableOpacity
                    style={styles.button}
                    onPress={() => this.action(job_id, "naviagateToJob")}
                  >
                    <Text style={[styles.btns, { marginRight: 6 }]}>
                      Navigate
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={styles.button}
                    onPress={() => this.action(job_id, "completed")}
                  >
                    <Text style={[styles.btns, { marginRight: 6 }]}>
                      Completed
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={styles.button}
                    onPress={() => this.action(job_id, "add_extra_item")}
                  >
                    <Text style={styles.btns}>+item</Text>
                  </TouchableOpacity>

                  {/* Finish */}
                  {/* {this.state.loading && this.state.data != undefined ? (
                    Object.keys(this.state.data).length > 0 &&
                    this.state.data.job_status != "accept" &&
                    this.state.data.job_status != "completed" ? (
                      <TouchableOpacity
                        style={styles.button}
                        onPress={() =>
                          this.action(this.state.data.id, "accept")
                        }
                      >
                        <Text style={styles.btnAccept}>Accept</Text>
                      </TouchableOpacity>
                    ) : Object.keys(this.state.data).length > 0 &&
                      this.state.data.job_status != "completed" &&
                      this.state.data.job_status != "pending" ? (
                      <TouchableOpacity
                        style={styles.button}
                        onPress={() =>
                          this.action(this.state.job_id, "completed")
                        }
                      >
                        <Text style={styles.btnAccept}>Completed</Text>
                      </TouchableOpacity>
                    ) : (
                      <Text></Text>
                    )
                  ) : (
                    <View>
                      {this.state.countforWardToLeadAcceptStatus == 0 ? (
                        <TouchableOpacity
                          style={styles.button}
                          onPress={() =>
                            this.action(this.state.job_id, "acceptfrdtolead")
                          }
                        >
                          <Text style={styles.btnAccept}>Accept</Text>
                        </TouchableOpacity>
                      ) : (
                        <TouchableOpacity style={styles.button}>
                          <Text style={styles.btnAccept}>Already Accepted</Text>
                        </TouchableOpacity>
                      )}
                    </View>
                  )} */}
                </View>
              </Card>
            </View>
          ) : (
            <ActivityIndicator size="small" color="#0000ff" />
          )}
        </ScrollView>
      </SafeAreaView>
    );
  }
}
