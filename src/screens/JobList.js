import React, { Component } from "react";
import {
  View,
  Text,
  ScrollView,
  SafeAreaView,
  TouchableOpacity,
  BackHandler,
  DeviceEventEmitter,
  ActivityIndicator,
} from "react-native";
import { Card } from "react-native-elements";
import AsyncStorage from "@react-native-async-storage/async-storage";

import { BASE_URL } from "../Config";
import Moment from "moment";

import styles from "../styles/Joblist";
import AppModal from "../components/AppModal";
import {
  _reject,
  _detail,
  getJobsList,
  getJobListOnEveryRequest,
} from "../functions";

// import AnimatedLoader from "react-native-animated-loader";

export default class JobList extends Component {
  _isMounted = false;

  prev_count = 0;
  prev_length;
  next_length;
  counter = 0;

  static navigationOptions = {
    title: "Job List",
    headerLeft: null,
    headerTitleStyle: {
      textAlign: "left",
      flex: 1,
    },
  };

  constructor() {
    super();
    this._isMounted = false;
    this.state = {
      user_email: "",
      list: [],
      action: "",
      changedVal: "Edit",
      data: [],
      data1: [],
      loading: false,
      selectedIndex: 2,
      renderToast: true,
      token: "",
      user_id: "",
      prev: 0,
      job_list_count: "",
      show_component: false,
      job_object: false,
      time: "",
      render: "",
    };
    AsyncStorage.multiGet(["token", "user_id"])
      .then((value) => {
        this.setState({
          token: value[0][1],
          user_id: value[1][1],
        });
        this.props.navigation.addListener("focus", () => {
          getJobsList(
            this.setState.bind(this),
            this.state.user_id,
            this.state.token
          );
        });
      })
      .done();
  }
  componentDidMount() {
    this._isMounted = true;
    this.getJobListOnEveryRequest();
  }

  componentWillUnmount() {
    this._isMounted = false;
    clearInterval(this.interval);
  }

  getJobListOnEveryRequest = () => {
    if (this._isMounted) {
      this.interval = setInterval(() => {
        AsyncStorage.getItem("job_list_count").then((job_list_count) => {
          this.setState({ job_list_count });
        });
        getJobListOnEveryRequest(
          this.setState.bind(this),
          this.state.user_id,
          this.state.token,
          this.state.job_list_count
        );
      }, 4000);
    }
    BackHandler.addEventListener("hardwareBackPress", function () {
      return true;
    });
  };

  action = (id, action) => {
    if (action == "reject") {
      _reject(
        this.setState.bind(this),
        id,
        this.state.token,
        this.state.user_id,
        this.props
      );
    }
    if (action == "detail") {
      _detail(id, this.props);
    }
    if (action == "accept") {
      this.acceptJob(id);
    }
  };
  acceptJob = (id) => {
    try {
      var token = this.state.token;
      var user_id = this.state.user_id;
      var $this = this;
      var OBJECT = {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Origin: "",
          Authorization: "Bearer " + token,
        },
        body: JSON.stringify({
          user_id,
        }),
      };
      fetch(`${BASE_URL}accept-job/${id}`, OBJECT)
        .then(function (res) {
          return res.json();
        })
        .then((res) => {
          this.props.navigation.navigate("Accepted Job", {
            screen: "Accept jobs",
          });
        })
        .catch(function (error) {
          // ADD THIS THROW error
          throw error;
        });
    } catch (err) {
      console.log(err);
    }
  };
  render() {
    Moment.locale("en");
    let subscription = DeviceEventEmitter.addListener(
      "hideComponent",
      (event) => {
        this.setState({ show_component: false });
      }
    );
    subscription.remove();
    return (
      <SafeAreaView style={styles.container}>
        <ScrollView style={styles.scrollView}>
          {this.state.loading ? (
            <View>
              {this.state.show_component ? (
                <AppModal
                  navigation={this.props.navigation}
                  jobitem={this.state.job_object}
                  showmodal={true}
                />
              ) : (
                <Text></Text>
              )}
              {this.state.data.map((l, index) => {
                var upload_job_time = "";
                var current_datetime = new Date();
                var current_day = current_datetime.getDate();
                var server_datetime = l.item.created_at;
                var created_day = Moment(server_datetime).format("D");
                var uploaded_time = Moment(server_datetime).format("LT");
                if (current_day == created_day) {
                  upload_job_time = "Today " + uploaded_time;
                } else if ((current_day == created_day) | +1) {
                  upload_job_time = "Yesterday " + uploaded_time;
                } else {
                  upload_job_time = uploaded_time;
                }
                var uploded_date_year = Moment(server_datetime).format("YY");
                var uploded_date_day = Moment(server_datetime).format("ddd");
                var uploded_date_month = Moment(server_datetime).format("MMM");
                return (
                  <TouchableOpacity
                    onPress={() => this.action(l.item.id, "detail")}
                    key={index}
                  >
                    <Card>
                      <Card.Title>{upload_job_time}</Card.Title>
                      <Card.Divider />
                      <View style={styles.cardHeader}>
                        <View style={styles.leftHeader}>
                          <Text
                            style={[styles.txtSize, { fontWeight: "bold" }]}
                          >
                            From: {l.item.from ?? ""}
                          </Text>
                          <Text style={styles.txtSize}>
                            {l.item.vehicle_required ?? ""}
                          </Text>
                          <Text style={styles.txtSize}>
                            {l.item.field_load ?? ""} :
                            {l.item.field_load == "Selfload" ? "Yes" : "No"}
                          </Text>
                          <Text
                            style={[styles.txtSize, { fontWeight: "bold" }]}
                          >
                            Adults: {l.item.text_adults ?? 0}
                          </Text>
                          <Text
                            style={[styles.txtSize, { fontWeight: "bold" }]}
                          >
                            Childrens: {l.item.text_children ?? 0}
                          </Text>
                        </View>

                        <View>
                          <Text style={styles.txtbold}>{uploded_date_day}</Text>
                          <Text style={styles.txtbold}>
                            {uploded_date_year}
                          </Text>
                          <Text style={styles.txtbold}>
                            {uploded_date_month}
                          </Text>
                        </View>
                        <View style={styles.rightHeader}>
                          <Text
                            style={[styles.txtSize, { fontWeight: "bold" }]}
                          >
                            {" "}
                            To: {l.item.to ?? ""}
                          </Text>
                          <Text
                            style={[styles.txtSize, { fontWeight: "bold" }]}
                          >
                            {" "}
                            Move Type
                          </Text>
                          {typeof l.item.job_items !== "undefined" &&
                            l.item.job_items.length > 0 &&
                            l.item.job_items.map((element, index1) => {
                              return (
                                <View key={index1}>
                                  <Text style={styles.txtSize}>
                                    {" "}
                                    {element.item ?? ""}
                                  </Text>
                                </View>
                              );
                            })}
                        </View>
                      </View>
                      <Text style={styles.timelimit}>Minimum 2 Hours</Text>
                      <View style={styles.jobinfo}>
                        <Text style={styles.jobinfo_txt}>
                          €
                          {(l.item.job_type == "Fixed"
                            ? parseFloat(l.item.item_total_amount) +
                              parseFloat(l.item.job_cost)
                            : 0) || 0}{" "}
                          {l.item.job_type == "Fixed"
                            ? "Fixed Amount"
                            : "AN HOUR"}
                        </Text>
                        <Text style={styles.jobinfo_txt}>
                          Name:{l.item.fullname ?? ""}
                        </Text>
                        <Text style={styles.jobinfo_txt}>
                          {l.item.contact_no ?? ""}
                        </Text>
                      </View>
                      <View style={styles.buttonGroup}>
                        <TouchableOpacity
                          style={styles.button}
                          onPress={() => this.action(l.item.id, "reject")}
                        >
                          <Text style={styles.btnReject}>REJECT</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={styles.button}
                          onPress={() => this.action(l.item.id, "accept")}
                        >
                          <Text style={styles.btnDetail}>Accept</Text>
                        </TouchableOpacity>
                        {/* <TouchableOpacity
                          style={styles.button}
                          onPress={() => this.action(l.item.id, "detail")}
                        >
                          <Text style={styles.btnDetail}>DETAIL</Text>
                        </TouchableOpacity> */}
                      </View>
                    </Card>
                  </TouchableOpacity>
                );
              })}
            </View>
          ) : (
            <ActivityIndicator size="small" color="#0000ff" />
            // <Text style={styles.loading}>Loading...</Text>
          )}
        </ScrollView>
      </SafeAreaView>
    );
  }
}
