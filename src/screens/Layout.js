import React, { Component } from "react";
import { View, StyleSheet } from "react-native";
class Layout extends Component {
  render() {
    return <View style={styles.container}>{this.props.children}</View>;
  }
}
export default Layout;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
  },
});
