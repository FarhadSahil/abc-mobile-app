import React, { useState, useEffect, Component } from "react";
import {
  Dimensions,
  ActivityIndicator,
  Platform,
  Text,
  View,
  StyleSheet,
} from "react-native";
import * as Location from "expo-location";
import MapView, { Marker } from "react-native-maps";
import MapViewDirections from "react-native-maps-directions";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { BASE_URL } from "../Config";

const { width, height } = Dimensions.get("window");
const ASPECT_RATIO = width / height;
const LATITUDE = 37.771707;
const LONGITUDE = -122.4053769;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

const GOOGLE_MAPS_APIKEY = "AIzaSyCGvbUdrsJOZuuHbD0q_J-BWkO5c8xhl40";
export default class LiveLocationTracker extends Component {
  _isMounted = false;

  constructor(props) {
    super(props);

    // AirBnB's Office, and Apple Park
    this.state = {
      location: "",
      latitude: "",
      longitude: "",
      dest_longitude: "",
      dest_latitude: "",
      destination_location: false,
      current_location: false,
      coordinates: [
        // {
        //   latitude: 34.02588408457759,
        //   longitude: 71.55996285534457,
        // },
        // {
        //   latitude: 24.90475221273429,
        //   longitude: 66.96029091216214,
        // },
      ],
    };

    this.mapView = null;
    AsyncStorage.multiGet(["token", "user_id"])
      .then((value) => {
        this.setState({ token: value[0][1], user_id: value[1][1] });
        const job_id = this.props.route.params.job_id;
        this.getJobsList(job_id);
      })
      .done();
  }
  async componentDidMount() {
    this._isMounted = true;
    this.watchId = Location.watchPositionAsync(
      {
        accuracy: Location.Accuracy.Balanced,
        timeInterval: 300,
        distanceInterval: 0,
      },
      (location) => {
        let coordinates = this.state.coordinates;
        coordinates[0] = {
          latitude: Number(location.coords.latitude),
          longitude: Number(location.coords.longitude),
        };

        this.setState({
          current_location: true,
          coordinates: coordinates,
        });
      }
    );
  }
  getJobsList(job_id) {
    try {
      var $this = this;
      var OBJECT = {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Origin: "",
          Authorization: "Bearer " + this.state.token,
        },
        body: JSON.stringify({
          user_id: this.state.user_id,
        }),
      };
      fetch(`${BASE_URL}job/${job_id}`, OBJECT)
        .then(function (res) {
          return res.json();
        })
        .then((data) => {
          var coordinates = this.state.coordinates;
          coordinates[1] = {
            latitude: Number(data.dest_lat),
            longitude: Number(data.dest_long),
          };
          this.setState({
            data: data,
            destination_location: true,
            job_id,
            coordinates: coordinates,
          });
        });
    } catch (err) {
      //console.log(err);
    }
  }
  onMapPress = () => {};
  componentWillUnmount() {
    this._isMounted = false;
    navigator.geolocation.clearWatch(this.watchId);
  }
  render() {
    if (this.state.current_location && this.state.destination_location) {
      return (
        <View>
          <MapView
            initialRegion={{
              latitude: LATITUDE,
              longitude: LONGITUDE,
              latitudeDelta: LATITUDE_DELTA,
              longitudeDelta: LONGITUDE_DELTA,
            }}
            style={{
              width,
              height,
            }}
            ref={(c) => (this.mapView = c)}
            onPress={() => this.onMapPress()}
          >
            {this.state.coordinates.map((coordinate, index) => (
              <MapView.Marker
                key={`coordinate_${index}`}
                coordinate={coordinate}
              />
            ))}
            {this.state.coordinates.length >= 0 && (
              <MapViewDirections
                origin={{
                  latitude: this.state.coordinates[0].latitude,
                  longitude: this.state.coordinates[0].longitude,
                }}
                destination={{
                  latitude: this.state.coordinates[1].latitude,
                  longitude: this.state.coordinates[1].longitude,
                }}
                apikey={GOOGLE_MAPS_APIKEY}
                strokeWidth={3}
                strokeColor="hotpink"
                onStart={(params) => {}}
                onReady={(result) => {
                  this.mapView.fitToCoordinates(result.coordinates, {
                    edgePadding: {
                      right: width / 20,
                      bottom: height / 20,
                      left: width / 20,
                      top: height / 20,
                    },
                  });
                }}
                onError={(errorMessage) => {}}
              />
            )}
          </MapView>
        </View>
      );
    } else {
      return <ActivityIndicator size="small" color="#0000ff" />;
    }
  }
}

// const styles = StyleSheet.create({ ... });
