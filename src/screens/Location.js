import React, { Component } from "react";
import { View, Text, TextInput, StyleSheet } from "react-native";
import MapView, { Marker } from "react-native-maps";
import MapViewDirections from "react-native-maps-directions";
import { Dimensions } from "react-native";
import AsyncStorage from "@react-native-async-storage/async-storage";

import * as Loc from "expo-location";
import { BASE_URL } from "../Config";

export default class Location extends Component {
  constructor(props) {
    super(props);
    this.state = {
      latitude: 35.681236,
      longitude: 139.767125,
      loading: false,
      token: "",
      user_id: "",
    };

    AsyncStorage.multiGet(["token", "user_id"])
      .then((value) => {
        this.setState({
          token: value[0][1],
          user_id: value[1][1],
        });
      })
      .done();
  }
  async componentDidMount() {
    try {
      let { status } = await Loc.requestForegroundPermissionsAsync();
      if (status !== "granted") {
        return;
      }
      let location = await Loc.getCurrentPositionAsync({});
      this.setState({
        latitude: location.coords.latitude,
        longitude: location.coords.longitude,
        loading: true,
      });
    } catch (error) {
      //console.log(error);
    }
    setInterval(() => {
      this.liveLocation();
    }, 10000);
  }

  liveLocation = () => {
    if (this.state.loading == true) {
      const data = {
        latitude: this.state.latitude,
        longitude: this.state.longitude,
        user_id: this.state.user_id,
      };
      fetch(`${BASE_URL}save-driver-live-location`, {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
        },
      })
        .then((response) => response.json())
        .then((res) => {})
        .catch((error) => {
          console.error(error);
        });
    }
  };
  render() {
    return (
      <View style={styles.container}>
        <MapView
          initialRegion={{
            latitude: parseFloat(
              this.state.loading ? this.state.latitude : 34.0151
            ),
            longitude: parseFloat(
              this.state.loading ? this.state.longitude : 71.5249
            ),

            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
          }}
          style={styles.map}
        >
          <Marker
            coordinate={{
              latitude: parseFloat(
                this.state.loading ? this.state.latitude : 34.0151
              ),
              longitude: parseFloat(
                this.state.loading ? this.state.longitude : 71.5249
              ),
            }}
          ></Marker>
        </MapView>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  map: {
    width: Dimensions.get("window").width,
    height: Dimensions.get("window").height / 2,
  },
});
