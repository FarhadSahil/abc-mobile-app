import React, { useState, useEffect, useRef } from "react";
import {
  StyleSheet,
  TextInput,
  View,
  Text,
  Image,
  TouchableOpacity,
  Platform,
} from "react-native";
import Firebase from "../../config/firebase";
import * as Notifications from "expo-notifications";

import { BASE_URL } from "../Config";
import styles from "../styles/Login";
const image = { uri: "https://reactjs.org/logo-og.png" };
import AsyncStorage from "@react-native-async-storage/async-storage";
import Constants from "expo-constants";
const { manifest } = Constants;
const api =
  typeof manifest.packagerOpts === `object` && manifest.packagerOpts.dev
    ? manifest.debuggerHost.split(`:`).shift().concat(`:3000`)
    : `api.example.com`;
const auth = Firebase.auth();

Notifications.setNotificationHandler({
  handleNotification: async () => ({
    shouldShowAlert: true,
    shouldPlaySound: false,
    shouldSetBadge: false,
  }),
});
export default function Login(props) {
  const [userId, setUserId] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [emailError, setEmailError] = useState("");
  const [passwordError, setPasswordError] = useState("");
  const [loginResponse, setloginResponse] = useState("");
  const notificationListener = useRef();
  const responseListener = useRef();

  async function registerForPushNotificationsAsync() {
    let token;
    if (Constants.isDevice) {
      const { status: existingStatus } =
        await Notifications.getPermissionsAsync();
      let finalStatus = existingStatus;
      if (existingStatus !== "granted") {
        const { status } = await Notifications.requestPermissionsAsync();
        finalStatus = status;
      }
      if (finalStatus !== "granted") {
        alert("Failed to get push token for push notification!");
        return;
      }
      token = (await Notifications.getExpoPushTokenAsync()).data;
      //console.log(token);
    } else {
      alert("Must use physical device for Push Notifications");
    }

    if (Platform.OS === "android") {
      Notifications.setNotificationChannelAsync("default", {
        name: "default",
        importance: Notifications.AndroidImportance.MAX,
        vibrationPattern: [0, 250, 250, 250],
        lightColor: "#FF231F7C",
      });
    }

    return token;
  }
  const loginFn = () => {
    setloginResponse("");
    setEmailError("");
    setPasswordError("");
    if (email == "") {
      setEmailError("Email is required");
      return;
    } else {
      setEmailError("");
    }
    if (password == "") {
      setPasswordError("Password is required");
      return;
    } else {
      setPasswordError("");
    }
    const data = {
      email: email,
      password: password,
      type: "driver",
    };
    fetch(`${BASE_URL}login`, {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    })
      .then((response) => response.json())
      .then((res) => {
        if (res.token) {
          if (res.expo_token == "" || res.expo_token == null) {
            registerForPushNotificationsAsync().then((token) => {
              registerToken(token, email);
            });
            notificationListener.current =
              Notifications.addNotificationReceivedListener((notification) => {
                setNotification(notification);
              });

            // This listener is fired whenever a user taps on or interacts with a notification (works when app is foregrounded, backgrounded, or killed)
            responseListener.current =
              Notifications.addNotificationResponseReceivedListener(
                (response) => {
                  props.navigation.navigate("Accepted Job", {
                    screen: "Job List",
                  });
                }
              );

            Notifications.removeNotificationSubscription(
              notificationListener.current
            );
            Notifications.removeNotificationSubscription(
              responseListener.current
            );
            // try {
            //   auth
            //     .signInWithEmailAndPassword(email, password)
            //     .then((loginUser) => {
            //       registerToken(loginUser);
            //     })
            //     .catch(function (signInError) {
            //       var signInError_obj = Object.assign({}, signInError);
            //       if (signInError_obj.code == "auth/wrong-password") {
            //         createUser(email, password);
            //       } else if (signInError_obj.code == "auth/too-many-requests") {
            //         setloginResponse(signInError_obj.message + "In firebase");
            //       } else if (signInError_obj.code == "auth/user-not-found") {
            //         createUser(email, password);
            //       }
            //     });
            // } catch (error) {
            //   setloginResponse(error);
            // }
          }
          AsyncStorage.setItem("token", res.token);
          AsyncStorage.setItem("user_id", JSON.stringify(res.user.id));
          props.navigation.navigate("Accepted Job", {
            screen: "Job List",
          });
        } else {
          setloginResponse(res.error);
        }
      })
      .catch((error) => {
        console.error(error);
      });
  };
  // function createUser(email, password) {
  //   auth
  //     .createUserWithEmailAndPassword(email, password)
  //     .then((loginUser) => {
  //       registerToken(loginUser);
  //     })
  //     .catch(function (error) {
  //       var err_obj = Object.assign({}, error);
  //       setloginResponse(err_obj.message + "In firebase");
  //     });
  // }
  function registerToken(expo_token, email) {
    const data = {
      email: email,
      modelId: Platform.OS,
      expo_token,
    };
    fetch(`${BASE_URL}check-expo-token`, {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    })
      .then((response) => response.json())
      .then((res) => {
        //console.log(res);
      })
      .catch((error) => {
        console.error(error);
      });
  }
  function navgiate() {
    AsyncStorage.multiGet(["user_id"])
      .then((value) => {
        if (value[0][1] != null) {
          props.navigation.navigate("Accepted Job", {
            screen: "Job List",
          });
        }
      })
      .done();
  }
  useEffect(() => {
    let ignore = false;
    if (!ignore) navgiate();
    return () => {
      ignore = true;
    };
  }, []);
  return (
    <View style={styles.container}>
      <View style={styles.image}>
        <Image
          source={{
            uri: "https://www.freeiconspng.com/uploads/driver-icon-4.png",
            width: 110,
            height: 100,
          }}
        />
      </View>
      <View style={styles.mainContent}>
        <Text style={styles.heading}>Welcome !</Text>
        {loginResponse != "" ? (
          <Text style={styles.errorText}>{loginResponse}</Text>
        ) : null}
        <TextInput
          onChangeText={(emailText) => setEmail(emailText)}
          style={styles.loginInput}
          placeholder="Email"
          autoCapitalize="none"
        />
        {emailError != "" ? (
          <Text style={styles.errorText}>{emailError}</Text>
        ) : null}
        <TextInput
          onChangeText={(passwordText) => setPassword(passwordText)}
          style={styles.loginInput}
          placeholder="Password"
          secureTextEntry={true}
          autoCapitalize="none"
        />
        {passwordError != "" ? (
          <Text style={styles.errorText}>{passwordError}</Text>
        ) : null}
        <TouchableOpacity style={styles.loginButton}>
          <Text style={styles.btnTextColor} onPress={() => loginFn()}>
            Login
          </Text>
        </TouchableOpacity>
      </View>

      <View>
        <TouchableOpacity style={styles.btnForgotPassword}>
          <Text style={styles.textForgotPassword}>Forgot password ?</Text>
          <Text style={styles.signupText}>
            Don't have an account{" "}
            <Text style={styles.textForgotPassword}>Sign Up</Text>
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}
