import React, { useEffect, useState } from "react";
import { Text } from "react-native";
import AsyncStorage from "@react-native-async-storage/async-storage";

function Logout(props) {
  const [userId, setUserId] = useState("");

  function LogoutUser() {
    AsyncStorage.multiGet(["user_id"])
      .then((value) => {
        if (value[0][1] != null) {
          AsyncStorage.clear();
          props.navigation.navigate("Login");
        }
      })
      .done();
  }
  useEffect(() => {
    let ignore = false;

    if (!ignore) LogoutUser();
    return () => {
      ignore = true;
    };
  }, []);
  return <Text></Text>;
}
export default Logout;
