import React, { Component } from "react";
import { StyleSheet, TextInput, View, Alert, Button, Text } from "react-native";
export default class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name: "",
      email: "",
      password: "",
      errors: "",
    };
  }

  loginUserFn = async () => {
    Alert.alert("login done");
  };
  insertRecordFn = () => {
    fetch("http://localhost/mobile/react-native-api/api/register", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        name: this.state.name,
        email: this.state.email,
        password: this.state.password,
      }),
    })
      .then((response) => response.json())
      .then((responseJson) => {
        //console.log(responseJson.danger);
        this.setState({ errors: responseJson.danger });
        if (responseJson.danger == undefined) {
          this.props.navigation.navigate("Login");
        }
        // Showing response message coming from server after inserting records.
      })
      .catch((error) => {
        console.error("farhadha", error);
      });
  };

  // componentDidMount(){
  //     this.loginUserFn();
  // }
  render() {
    return (
      <>
        {this.state.errors && <h3 className="error"> {this.state.errors} </h3>}
        <TextInput
          placeholder="Enter User Name"
          onChangeText={(nameVal) => this.setState({ name: nameVal })}
          underlineColorAndroid="transparent"
          style={styles.TextInputStyleClass}
        />
        <TextInput
          placeholder="Enter User Email"
          onChangeText={(emailVal) => this.setState({ email: emailVal })}
          underlineColorAndroid="transparent"
          style={styles.TextInputStyleClass}
        />
        <TextInput
          placeholder="Enter User Password"
          onChangeText={(passwordVal) =>
            this.setState({ password: passwordVal })
          }
          underlineColorAndroid="transparent"
          style={styles.TextInputStyleClass}
          secureTextEntry={true}
        />
        <View style={styles.btnMainView}>
          <Button
            onPress={this.insertRecordFn}
            title="Register"
            color="#2196F3"
          />
          <View style={styles.containerRegister}>
            <Button
              color="red"
              title="Login"
              onPress={() => this.props.navigation.navigate("Login")}
            />
          </View>
        </View>
      </>
    );
  }
}

const styles = StyleSheet.create({
  TextInputStyleClass: {
    borderColor: "gray",
    borderWidth: 1,
    marginBottom: 20,
    height: 40,
    width: "80%",
    textAlign: "center",
    marginLeft: "10%",
  },
  btnMainView: {
    flexDirection: "row",
    margin: 10,
    padding: 10,
    marginLeft: "30%",
  },
  containerRegister: {
    marginLeft: 10,
  },
});
