import React, { Component } from "react";
import Layout from "./Layout";
import {
  StyleSheet,
  TextInput,
  View,
  Alert,
  Button,
  Text,
  Image,
  TouchableOpacity,
  StatusBar,
  SafeAreaView,
  ScrollView,
} from "react-native";
const image = { uri: "https://reactjs.org/logo-og.png" };
import AsyncStorage from "@react-native-async-storage/async-storage";

export default class Splash extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      emailError: "",
      passwordError: "",
      loginResponse: "",
    };
  }
  navigateBetween = (action) => {
    if (action == "driver") {
      this.props.navigation.navigate("Login");
    }
    if (action == "survey") {
      this.props.navigation.navigate("Survey Login");
    }
  };
  render() {
    return (
      <Layout>
        <View style={styles.image}>
          {/* <Image
            source={{
              uri: "http://assets.stickpng.com/images/58aff1cf829958a978a4a6cb.png",
              width: 220,
              height: 120,
            }}
          /> */}
          <Image
            source={require("../../assets/welcome.jpg")}
            style={{ width: 300, height: 120 }}
          />
        </View>
        <View style={styles.mainContent}>
          <TouchableOpacity style={styles.loginButton}>
            <Text
              style={styles.btnTextColor}
              onPress={() => this.navigateBetween("survey")}
            >
              Survey Login
            </Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.loginButton}>
            <Text
              style={styles.btnTextColor}
              onPress={() => this.navigateBetween("driver")}
            >
              Driver Login
            </Text>
          </TouchableOpacity>
        </View>
      </Layout>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  image: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: -120,
  },
  mainContent: {
    flex: 1,
    marginLeft: 20,
    marginTop: -120,
  },
  loginButton: {
    width: 90 + "%",
    backgroundColor: "#f15a36",
    padding: 15,
    borderRadius: 20,
    textAlign: "center",
    marginTop: 15,
  },
  btnTextColor: {
    color: "#fff",
    fontSize: 20,
    textAlign: "center",
  },
  btnForgotPassword: {
    fontSize: 20,
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  textForgotPassword: {
    color: "#f15a36",
    marginTop: -100,
  },
  signupText: {
    marginTop: 20,
    color: "#f15a36",
  },
  errorText: {
    color: "#f15a36",
    fontSize: 16,
    marginBottom: 10,
  },
});
