import React, { Component } from "react";
import { View, Text, StyleSheet } from "react-native";
import { WebView } from "react-native-webview";
import { BASE_URL } from "../Config/index";

export default class Survey extends Component {
  render() {
    var webview_url = BASE_URL.replace("/api/", " ");
    webview_url = webview_url.trim();
    return (
      <View style={{ flex: 1, flexDirection: "column" }}>
        <WebView
          source={{
            uri: `${webview_url}/survey#personal`,
          }}
          style={{ marginTop: 20 }}
        />
      </View>
    );
  }
}

const style = StyleSheet.create({});
