import React, { Component } from "react";
import { TextInput, View, Text, Image, TouchableOpacity } from "react-native";
import { BASE_URL } from "../Config";

import styles from "../styles/Login";
const image = { uri: "https://reactjs.org/logo-og.png" };
import AsyncStorage from "@react-native-async-storage/async-storage";

export default class SurveyLogin extends Component {
  constructor() {
    super();
    this.state = {
      email: "",
      password: "",
      emailError: "",
      passwordError: "",
      loginResponse: "",
    };
  }
  login = () => {
    if (this.state.email == "") {
      this.setState({ emailError: "Email is required" });
      return;
    } else {
      this.setState({ emailError: "" });
    }
    if (this.state.password == "") {
      this.setState({ passwordError: "Password is required" });
      return;
    } else {
      this.setState({ passwordError: "" });
    }
    const data = {
      email: this.state.email,
      password: this.state.password,
    };
    fetch(`${BASE_URL}survey-login`, {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
        // 'Content-Type': 'application/x-www-form-urlencoded',
      },
    })
      .then((response) => response.json())
      .then((res) => {
        if (res.token) {
          AsyncStorage.setItem("token", res.token);
          AsyncStorage.setItem("user_id", JSON.stringify(res.user.id));
          this.props.navigation.navigate("Survey");
        } else {
          this.setState({ loginResponse: res.error });
        }
      })
      .catch((error) => {
        console.error(error);
      });
  };
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.image}>
          <Image
            source={{
              uri: "https://www.searates.com/design/images/about/icons/survey.png",
              width: 110,
              height: 100,
            }}
          />
        </View>
        <View style={styles.mainContent}>
          <Text style={styles.heading}>Welcome !</Text>
          {this.state.loginResponse != "" ? (
            <Text style={styles.errorText}>{this.state.loginResponse}</Text>
          ) : null}
          <TextInput
            onChangeText={(emailText) => this.setState({ email: emailText })}
            style={styles.loginInput}
            placeholder="Email"
          />
          {this.state.emailError != "" ? (
            <Text style={styles.errorText}>{this.state.emailError}</Text>
          ) : null}
          <TextInput
            onChangeText={(passwordText) =>
              this.setState({ password: passwordText })
            }
            style={styles.loginInput}
            placeholder="Password"
            secureTextEntry={true}
          />
          {this.state.passwordError != "" ? (
            <Text style={styles.errorText}>{this.state.passwordError}</Text>
          ) : null}
          <TouchableOpacity style={styles.loginButton}>
            <Text style={styles.btnTextColor} onPress={this.login}>
              Login
            </Text>
          </TouchableOpacity>
        </View>

        <View>
          <TouchableOpacity style={styles.btnForgotPassword}>
            <Text style={styles.textForgotPassword}>Forgot password ?</Text>
            <Text style={styles.signupText}>
              Don't have an account{" "}
              <Text style={styles.textForgotPassword}>Sign Up</Text>
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
