import React, { Component } from "react";
import { View } from "react-native";
import { StyleSheet, Dimensions } from "react-native";
import * as Location from "expo-location";
import MapView, { Marker, Polyline, PROVIDER_GOOGLE } from "react-native-maps";
import * as Permissions from "expo-permissions";
import * as TaskManager from "expo-task-manager";
const LOCATION_TASK_NAME = "background-location-task";

const LATITUDE_DELTA = 0.009;
const LONGITUDE_DELTA = 0.009;
const LATITUDE = 18.7934829;
const LONGITUDE = 98.9867401;
export default class UserLocationTracker extends Component {
  constructor(props) {
    super(props);
    this.state = {
      latitude: LATITUDE,
      longitude: LONGITUDE,
      error: null,
      routeCoordinates: [],
    };
  }
  componentDidMount = async () => {
    let { status } = await Location.requestForegroundPermissionsAsync();
    if (status === "granted") {
      this._getLocationAsync();
    } else {
      this.setState({ error: "Locations services needed" });
    }
    let position = await Location.getCurrentPositionAsync({});
    this.setState({
      latitude: position.coords.latitude,
      longitude: position.coords.longitude,
      error: null,
    });
  };
  _getLocationAsync = async () => {
    await Location.startLocationUpdatesAsync("background-location-task", {
      enableHighAccuracy: true,
      distanceInterval: 1,
      timeInterval: 5000,
    });
    // watchPositionAsync Return Lat & Long on Position Change
    this.location = await Location.watchPositionAsync(
      {
        enableHighAccuracy: true,
        distanceInterval: 1,
        timeInterval: 10000,
      },
      (newLocation) => {
        let { coords } = newLocation;
        // console.log(coords);
        let region = {
          latitude: coords.latitude,
          longitude: coords.longitude,
          latitudeDelta: 0.045,
          longitudeDelta: 0.045,
        };
        this.setState({ region: region });
      },
      (error) => {
        //  console.log(error);
      }
    );
    return this.location;
  };
  getMapRegion = () => ({
    latitude: this.state.latitude,
    longitude: this.state.longitude,
    latitudeDelta: LATITUDE_DELTA,
    longitudeDelta: LONGITUDE_DELTA,
  });
  render() {
    TaskManager.defineTask(LOCATION_TASK_NAME, async ({ data, error }) => {
      if (error) {
        //  console.log(error);
        return;
      }
      if (data) {
        const { locations } = data;
        let latitude = locations[0].coords.latitude;
        let longitude = locations[0].coords.longitude;

        const { routeCoordinates } = this.state;
        const newCoordinate = { latitude, longitude };
        this.setState({
          latitude,
          longitude,
          routeCoordinates: routeCoordinates.concat([newCoordinate]),
        });
      }
    });
    return (
      <View style={styles.container}>
        <MapView
          style={styles.map}
          provider={PROVIDER_GOOGLE}
          region={this.getMapRegion()}
        >
          <Polyline coordinates={this.state.routeCoordinates} strokeWidth={5} />
          <Marker coordinate={this.getMapRegion()} />
        </MapView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
});
