import React, { Component } from "react";
import { View, Text } from "react-native";

export default class ViewScreen extends Component {
  render() {
    return (
      <View>
        <Text>This is View </Text>
      </View>
    );
  }
}
