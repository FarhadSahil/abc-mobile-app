import { StyleSheet, Dimensions } from "react-native";

export default StyleSheet.create({
  container: {},
  scrollView: {
    backgroundColor: "#EEF1F4",
    marginHorizontal: 20,
  },
  text: {
    fontSize: 42,
  },
  buttonGroup: {
    flex: 1,
    flexDirection: "row",
    padding: 0,
    // marginHorizontal: 20,
    justifyContent: "space-between",
  },
  button: {
    width: 100,
    height: 50,
    marginTop: 10,
    fontSize: 20,
  },
  btnSuccess: {
    textAlign: "center",
    padding: 10,
    color: "white",
    backgroundColor: "#42ba96",
    borderRadius: 10,
    marginRight: 5,
  },
  btnAccept: {
    textAlign: "center",
    padding: 10,
    color: "white",
    backgroundColor: "#007bff",
    borderRadius: 10,
    marginLeft: 5,
  },
  loading: {
    fontSize: 12,
    flex: 1,
    justifyContent: "center",
    textAlign: "center",
    backgroundColor: "red",
    width: 100 + "%",
    height: 30,
    lineHeight: 30,
    color: "white",
    borderRadius: 3,
    marginTop: 10,
  },
  map: {
    width: Dimensions.get("window").width,
    height: Dimensions.get("window").height / 2,
  },
  btns: {
    borderWidth: 1,
    padding: 4,
    marginRight: 30,
    textAlign: "center",
  },
});
