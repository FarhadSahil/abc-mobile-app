import { StyleSheet } from "react-native";

export default StyleSheet.create({
  container: {},
  scrollView: {
    backgroundColor: "#EEF1F4",
    marginHorizontal: 3,
  },
  text: {
    fontSize: 42,
  },
  buttonGroup: {
    flex: 1,
    flexDirection: "row",
    // padding: 10,
    justifyContent: "space-between",
  },
  button: {
    width: 100,
    height: 50,
    marginTop: 10,
    fontSize: 20,
    marginRight: -5,
  },
  btnReject: {
    textAlign: "center",
    padding: 10,
    color: "white",
    backgroundColor: "#df4759",
    borderRadius: 10,
    marginRight: 5,
  },
  btnDetail: {
    textAlign: "center",
    padding: 10,
    color: "white",
    backgroundColor: "#007bff",
    borderRadius: 10,
    marginLeft: 5,
  },
  loading: {
    fontSize: 12,
    flex: 1,
    textAlign: "center",
    backgroundColor: "red",
    width: 100 + "%",
    height: 30,
    lineHeight: 30,
    color: "white",
    borderRadius: 3,
    marginTop: 10,
  },
  jobinfo: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "space-between",
    marginTop: 10,
  },
  jobinfo_txt: {
    backgroundColor: "#3caed8",
    color: "white",
    padding: 10,
    borderWidth: 1,
    borderRadius: 6,
    borderColor: "white",
    textAlign: "center",
  },
  timelimit: {
    fontSize: 20,
    textAlign: "center",
    marginTop: 10,
    color: "grey",
  },
  txtbold: {
    color: "black",
    fontWeight: "bold",
    textAlign: "center",
    fontSize: 12,
  },
  cardHeader: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
  },
  txtSize: {
    fontSize: 16,
  },
  leftHeader: {
    flex: 1,
    flexDirection: "column",
    marginBottom: 10,
  },
  rightHeader: {
    flex: 1,
    flexDirection: "column",
    marginBottom: 10,
    marginLeft: 15,
  },
});
