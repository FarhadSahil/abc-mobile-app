import { StyleSheet } from "react-native";
export default StyleSheet.create({
  container: {
    flex: 1,
  },
  image: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: -120,
  },
  mainContent: {
    flex: 1,
    marginLeft: 20,
    marginTop: -120,
  },
  heading: {
    fontSize: 20,
    fontWeight: "bold",
    paddingBottom: 20,
  },
  loginInput: {
    borderWidth: 2, // size/width of the border
    borderColor: "lightgrey", // color of the border
    paddingLeft: 10,
    paddingRight: 10,
    height: 50,
    width: 90 + "%",
    marginBottom: 10,
    borderRadius: 10,
    backgroundColor: "#EBEBEB",
  },
  loginButton: {
    width: 90 + "%",
    backgroundColor: "#f15a36",
    padding: 15,
    borderRadius: 20,
    textAlign: "center",
    marginTop: 15,
  },
  btnTextColor: {
    color: "#fff",
    fontSize: 20,
    textAlign: "center",
  },
  btnForgotPassword: {
    fontSize: 20,
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  textForgotPassword: {
    color: "#f15a36",
    marginTop: -100,
  },
  signupText: {
    marginTop: 20,
    color: "#f15a36",
  },
  errorText: {
    color: "#f15a36",
    fontSize: 16,
    marginBottom: 10,
  },
  camera: {
    ...StyleSheet.absoluteFillObject,
  },
  removeButton: {
    fontSize: 20,
    padding: 20,
  },
});
